#!/bin/sh

BTB=./btb   #bin->txt->bin sequence folder
PRL=0-spl   #Spell_list base filename
DRL=1-orl   #Soell_list+Orb_list base filename
ITL=2-itl   #Item_list base filename
FLL=3-bgl   #Item_list+Bag_list base filename
CLL=4-cll   #Player_classes base filename
PCL=5-pcl   #Player_classes+Pch_list base filename

# Extensions for test files (must match what's defined in the .c source)
eb1=bin1    #Original
eb2=bin2    #Final bin
et=txt      #Generated txt

echo bin to txt to bin Test script
echo
echo Removing old files
rm $BTB/*.$eb2
rm $BTB/*.$et
echo

echo Testing Spell_list
./test_btb $BTB/$PRL <0.in
diff -s $BTB/$PRL.$eb1 $BTB/$PRL.$eb2
echo
echo

echo Testing Spell_list+Orb_list
./test_btb $BTB/$DRL <0.in
diff -s $BTB/$DRL.$eb1 $BTB/$DRL.$eb2
echo
echo

echo Testing Item_list
./test_btb $BTB/$ITL <1.in
diff -s $BTB/$ITL.$eb1 $BTB/$ITL.$eb2
echo
echo

echo Testing Item_list+Bag_list
./test_btb $BTB/$FLL <1.in
diff -s $BTB/$FLL.$eb1 $BTB/$FLL.$eb2
echo
echo

echo Testing Player_classes
./test_btb $BTB/$CLL <2.in
diff -s $BTB/$CLL.$eb1 $BTB/$CLL.$eb2
echo
echo

echo Testing Player_classes+Pch_list
./test_btb $BTB/$PCL <2.in
diff -s $BTB/$PCL.$eb1 $BTB/$PCL.$eb2
echo