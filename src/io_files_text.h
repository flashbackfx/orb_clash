/* io_files_text.h */

#ifndef IO_FILES_TEXT_H
#define IO_FILES_TEXT_H

#include "adt/spell_list_adt.h"
#include "adt/orb_list_adt.h"
#include "adt/item_list_adt.h"
#include "adt/bag_list_adt.h"
#include "adt/player_classes_adt.h"
#include "adt/pch_list_adt.h"

int io_text_load_spells_orbs_file(const char *filename,
                                  Spell_list *spell_list_ptr,
                                  Orb_list *orb_list_ptr);
int io_text_load_items_bags_file(const char *filename, Item_list *item_list_ptr,
                                 Bag_list *bag_list_ptr);
int io_text_load_classes_chars_file(const char *filename,
                                    Player_classes *pc_ptr,
                                    Pch_list *pl_chars_ptr);

long io_text_save_spells_orbs_file(const char *filename,
                                   Spell_list spell_list,
                                   Orb_list orb_list);
long io_text_save_items_bags_file(const char *filename, Item_list item_list,
                                  Bag_list bag_list);
long io_text_save_classes_chars_file(const char *filename, Player_classes pc,
                                     Pch_list pl_chars);

#endif

/**
FILE FORMAT DOCUMENTATION:

These functions parse a text file formatted in a predefined way to extract
editor-related data.

Sections:
 Each file can have one or two sections. The second section uses information
from the first section. Primary sections are used for Spell_list, Item_list
and Player_classes data. Its respective optional second sections store
Orb_list, Bag_list and Player_character_list data.

Headers:
 Eight characters at the beginning of a section that inform the type of data
the section holds. These are the defined headers:
	Primary sections:             Secondary sections:
	spellist -> Spell_list        orb.list -> Orb_list
	itemlist -> Item_list         bag.list -> Bag_list
	classes. -> Player_classes    charlist -> Player_character_list

Element:
 Values enclosed in brackets ('"'). They can represent either integers,
floating point numbers or strings. Integers can be signed or unsigned
depending on the data being read. Floating point numbers must be positive,
and cannot be expressed using the character e. There is a limit on the digits
read for numerical values. Strings can contain any character (even control
characters) except brackets ('"') (which would mark the end of the element,
there are no escape codes).

Object element group:
 Collection of elements that define an object, ie. a Spell object in a
Spell_list. Elements can be separated by whitespace between them. Object
element gropus are separated each by a comma (','). Each object element group
has a layout which indicates how many elements are read and how its data is
iterpreted. The layouts are as follows (i means integer value, f means
floating point value, s means string):
	Spell (in Spell_list):
		"(i) cost" "(s) name" "(s) description"
	Orb (in Orb_list):
		"(s) name" "(i) 1st spell in orb" ... "(i) Nth spell in orb"
	Item (in Item_list):
		"(i) size" "(s) name" "(s) description"
	Bag (in Bag_list):
		"(i) size" "(s) name" "(i) 1st item in bag" ... "(i) Nth item in bag"
	player_class (in Player_classes):
		"(i) type" "(f) ATK" "(f) VEL" "(f) SYN" "(f) RAM" "(f) DEF" "(f) VIT" "(f) vit_floor" "(s) name"
	Player_character (in Pch_list):
		"(i) class" "(i) level" "(s) name"
 References in secondary sections are done to objects in the corresponding
primary section by its position on the list. Orbs reference Spells in
the Spell_list, Bags reference Items in the Item_list and
Player_characters reference classes in the Player_classes list.
 There can be an arbitrary number of Spells in a Orb and of Items in a
Bag. In these cases, the parser keeps reading elements until the end of
object element group indicator, a comma (',').
 In the rest of cases, after having read the defined amount of elements for
the current object element group, the parser ignores all subsequent characters
until it finds a comma (',') a semicolon (';') or the end of file. As a side
effect, it is possible to embed comments between the last valid element and
the end of object element group indicator, the end of section indicator or
the end of file (whatever comes next).

 Special characters:
' ; '  End of section indicator: Marks the beginning of a secondary section.
' , '  End of object element group: Marks the beginning of the next object
      element group (do not use before the first).
' " '  Element enclosure: Marks the beginning and the end of element data in
      the file.

********/


/**
* LOAD FUNCTIONS
*/

/**
* * int io_text_load_spells_orbs_file(const char *filename,
                                      Spell_list *spell_list_ptr,
                                      Orb_list *orb_list_ptr) *
* * int io_text_load_items_bags_file(const char *filename,
                                     Item_list *item_list_ptr,
                                     Bag_list *bag_list_ptr) *
* * io_text_load_classes_chars_file(const char *filename,
                                    Player_classes *pc_ptr,
                                    Pch_list *pl_chars_ptr) *
*
* These functions open the file in the path given by 'filename' as text
* and parse it according to the specification given above. The relevant
* objects are created in the locations pointed to by the second argument
* and, if the opened file has a valid secondary section, the third
* argument. If the third argument is NULL no attempt at reading a
* secondary section will be made. Objects for the lists pointed to by
* the second and third arguments are created as valid elements are read
* from the file.
* Returns: 0, or a negative number on error.
*/


/**
* SAVE FUNCTIONS
*/

/**
* * long io_text_save_spells_orbs_file(const char *filename,
                                       Spell_list spell_list,
                                       Orb_list orb_list) *
* * long io_text_save_items_bags_file(const char *filename,
                                      Item_list item_list,
                                      Bag_list bag_list) *
* * long io_text_save_classes_chars_file(const char *filename,
                                         Player_classes pc,
                                         Pch_list pl_chars) *
*
* These functions create a file in the path given by 'filename' as text
* and write a primary section with data from the object pointed to by
* the second argument and a secondary section with data from the object
* pointed to by the third argument if it is not NULL. The file is
* written following a format compilant with the specification given
* above.
* Returns: The result of ftell at the end of the saved file, or a
*  negative number on error.
*/