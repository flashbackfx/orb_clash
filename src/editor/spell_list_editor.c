/* spell_list_editor.c */

#include "editor_public.h"
#include "../adt/spell_adt.h"
#include "../adt/spell_list_adt.h"
#include "../adt/orb_list_adt.h"
#include "../io_files.h"
#include "../io_files_text.h"
#include "../readline.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

static Spell_list option_add_spell(Spell_list spell_list);
static void option_display_list(Spell_list spell_list);
static void option_edit_spell(Spell_list spell_list);
static Spell_list option_clear_list(Spell_list spell_list,
                                    Orb_list orb_list);
static void option_save_list(Spell_list spell_list);

Spell_list spell_list_editor(Spell_list spell_list, Orb_list orb_list)
{
	enum {BACK = 0, ADD, REMOVE, LIST, EDIT, CLEAR, SAVE};
	int option;

	for (;;) {
		printf("\nSpell list editor\n");
		printf("(%d-Add, %d-Remove, %d-Display list, %d-Edit spell,\n"
		       " %d-Clear, %d-Save, %d-Back)\n:",
		       ADD, REMOVE, LIST, EDIT, CLEAR, SAVE, BACK);
		option = flushed_scanf_d(FSD_ERR_VAL);

		switch (option) {
		case BACK:
			return spell_list;

		case ADD:
			spell_list = option_add_spell(spell_list);
			break;

		case REMOVE:
			ep_option_remove_spell(spell_list, orb_list);
			break;

		case LIST:
			option_display_list(spell_list);
			break;

		case EDIT:
			option_edit_spell(spell_list);
			break;

		case CLEAR:
			spell_list = option_clear_list(spell_list, orb_list);
			break;

		case SAVE:
			option_save_list(spell_list);
			break;

		default:
			printf("Option %d not recognised\n", option);
			break;
		}

		// Destroy empty list
		if (spell_list != NULL && spell_list_total(spell_list) == 0) {
			spell_list = spell_list_destroy(spell_list);  // spell_list becomes NULL
		}
	}
}

static Spell_list option_add_spell(Spell_list spell_list)
{
	Spell spell;
	int cost_in_mp;
	char name[SPELL_NAME_LEN+1];
	char description[DESCRIPTION_MAX+1];

	printf("Enter spell name: ");
	read_line(name, SPELL_NAME_LEN);

	printf("Enter cost in MPT: ");
	cost_in_mp = flushed_scanf_d(FSD_ERR_VAL);
	if (cost_in_mp < 0) {
		printf("Cost must be positive\n");
		return spell_list;
	}

	printf("Enter description:\n");
	read_line(description, DESCRIPTION_MAX);

	printf("Is this correct? (y)\n");
	printf("Name: %s\nCost: %d\nDescription: %s\n:", name, cost_in_mp,
	       description);
	if (accept_prompt('y') == false) {
		return spell_list;
	}

	if (spell_list == NULL) {
		spell_list = spell_list_create();
	}
	if (spell_list != NULL) {
		spell = spell_create(name, description, cost_in_mp);
		spell_list_add(spell_list, spell);
	}

	return spell_list;
}

static void option_edit_spell(Spell_list spell_list)
{
	if (spell_list == NULL) {
		printf("Spell list not defined yet!\n");
		return;
	}

	Spell spell;
	int choice;

	spell = ep_choose_spell_from_list_obj(spell_list, "edit");
	if (spell == NULL) {
		return;
	}

	for (;;) {
		enum {BACK = 0, CHANGE_COST, CHANGE_NAME, CHANGE_DESCRIPTION,
		                SHOW_INFO};

		printf("\nEditing spell %s\n", spell_get_name(spell));
		printf("(%d-Change cost, %d-Change name, %d-Change description,\n"
		       " %d-Show spell info, %d-Back)\n:",
		       CHANGE_COST, CHANGE_NAME, CHANGE_DESCRIPTION, SHOW_INFO, BACK);
		choice = flushed_scanf_d(FSD_ERR_VAL);

		switch (choice) {
		case BACK:
			return;

		case CHANGE_COST:
			printf("Current cost: %d\nEnter new cost: ", spell_get_cost(spell));
			{
				int new_cost = flushed_scanf_d(FSD_ERR_VAL);

				if (new_cost < 1) {
					printf("Cost must be positive\n");
					break;
				}
				spell_set_cost(spell, new_cost);
			}
			break;

		case CHANGE_NAME:
			{
				char new_name[SPELL_NAME_LEN+1];

				printf("Enter new name: ");
				read_line(new_name, SPELL_NAME_LEN);
				spell_set_name(spell, new_name);
			}
			break;

		case CHANGE_DESCRIPTION:
			printf("Enter new description: ");
			{
				char new_description[DESCRIPTION_MAX+1];

				read_line(new_description, DESCRIPTION_MAX);
				spell_set_description(spell, new_description);
			}
			break;

		case SHOW_INFO:
			ep_show_spell_info(spell, true, true);
			break;

		default:
			printf("Option %d not recognised\n", choice);
			break;
		}
	}
}

static void option_display_list(Spell_list spell_list)
{
	if (spell_list == NULL) {
		printf("Spell list not defined yet!\n");
		return;
	}

	printf("Show descriptions? (y): ");
	ep_show_spell_list(spell_list, accept_prompt('y'));
}

static Spell_list option_clear_list(Spell_list spell_list,
                                    Orb_list orb_list)
{
	if (spell_list != NULL) {
		if (orb_list != NULL) {
			orb_list_update_removed_spells(orb_list, NULL, 0);  // Empty all orbs
		}

		spell_list_destroy(spell_list);
	}

	return NULL;
}

static void option_save_list(Spell_list spell_list)
{
	if (spell_list == NULL) {
		printf("Spell list not defined yet!\n");
		return;
	}
	if (spell_list_total(spell_list) < 1) {
		printf("Spell list is empty\n");
		return;
	}

	char filename[FILENAME_MAX];

	printf("Enter filename: ");
	read_line(filename, FILENAME_MAX - 1);

	if (filename[0] == '\0') {
		printf("Filename cannot be empty\n");
		return;
	}

	printf("Save as text (t) or binary?\n:");
	if (accept_prompt('t') == true) {
		io_text_save_spells_orbs_file(filename, spell_list, NULL);
	}
	else {
		io_save_spells_orbs_file(filename, spell_list, NULL);
	}
}