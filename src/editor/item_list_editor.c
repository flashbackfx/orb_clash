/* item_list_editor.c */

#include "editor_public.h"
#include "../adt/g_item_adt.h"
#include "../adt/item_list_adt.h"
#include "../io_files.h"
#include "../io_files_text.h"
#include "../readline.h"
#include <stdio.h>
#include <stdlib.h>

static Item_list option_add_item(Item_list item_list);
static void option_display_list(Item_list item_list);
static void option_edit_item(Item_list item_list);
static Item_list option_clear_list(Item_list item_list, Bag_list bag_list);
static void option_save_list(Item_list item_list);

Item_list item_list_editor(Item_list item_list, Bag_list bag_list)
{
	enum {BACK = 0, ADD, REMOVE, LIST, EDIT, CLEAR, SAVE};
	int option;

	for (;;) {
		printf("\nItem list editor\n");
		printf("(%d-Add, %d-Remove, %d-Display list, %d-Edit item, %d-Clear,\n"
		       " %d-Save, %d-Back)\n:",
		       ADD, REMOVE, LIST, EDIT, CLEAR, SAVE, BACK);
		option = flushed_scanf_d(FSD_ERR_VAL);

		switch (option) {
		case BACK:
			return item_list;

		case ADD:
			item_list = option_add_item(item_list);
			break;

		case REMOVE:
			ep_option_remove_item(item_list, bag_list);
			break;

		case LIST:
			option_display_list(item_list);
			break;

		case EDIT:
			option_edit_item(item_list);
			break;

		case CLEAR:
			item_list = option_clear_list(item_list, bag_list);
			break;

		case SAVE:
			option_save_list(item_list);
			break;

		default:
			printf("Option %d not recognised\n", option);
			break;
		}

		// Destroy empty item list
		if (item_list != NULL && item_list_total(item_list) == 0) {
			item_list = item_list_destroy(item_list);  // item_list becomes NULL
		}
	}
}

static Item_list option_add_item(Item_list item_list)
{
	Item item;
	int size_in_bag;
	char name[ITEM_NAME_LEN+1];
	char description[DESCRIPTION_MAX+1];

	printf("Enter item name: ");
	read_line(name, ITEM_NAME_LEN);

	printf("Enter size in Bag: ");
	size_in_bag = flushed_scanf_d(FSD_ERR_VAL);
	if (size_in_bag < 0) {
		printf("Size must be positive\n");
		return item_list;
	}

	printf("Enter description:\n");
	read_line(description, DESCRIPTION_MAX);

	printf("Is this correct? (y)\n");
	printf("Name: %s\nSize: %d\nDescription: %s\n:", name, size_in_bag,
	       description);
	if (accept_prompt('y') == false) {
		return item_list;
	}

	if (item_list == NULL) {
		item_list = item_list_create();
	}
	if (item_list != NULL) {
		item = item_create(name, description, size_in_bag);
		item_list_add(item_list, item);
	}

	return item_list;
}

static void option_display_list(Item_list item_list)
{
	if (item_list == NULL) {
		printf("Item list not defined yet!\n");
		return;
	}

	printf("Show descriptions? (y): ");
	ep_show_item_list(item_list, accept_prompt('y'));
}

static void option_edit_item(Item_list item_list)
{
	if (item_list == NULL) {
		printf("Item list not defined yet!\n");
		return;
	}

	Item item;
	int choice;

	item = ep_choose_item_from_list_obj(item_list, "edit");
	if (item == NULL) {
		return;
	}

	for (;;) {
		enum {BACK = 0, CHANGE_SIZE, CHANGE_NAME, CHANGE_DESCRIPTION,
		                SHOW_INFO};

		printf("\nEditing item %s\n", item_get_name(item));
		printf("(%d-Change size, %d-Change name, %d-Change description,\n"
		       " %d-Show item info, %d-Back)\n:",
		       CHANGE_SIZE, CHANGE_NAME, CHANGE_DESCRIPTION, SHOW_INFO, BACK);
		choice = flushed_scanf_d(FSD_ERR_VAL);

		switch (choice) {
		case BACK:
			return;

		case CHANGE_SIZE:
			printf("Current size: %d\nEnter new size: ", item_get_size(item));
			{
				int new_size = flushed_scanf_d(FSD_ERR_VAL);

				if (new_size < 1) {
					printf("Size must be positive\n");
					break;
				}
				item_set_size(item, new_size);
			}
			break;

		case CHANGE_NAME:
			{
				char new_name[ITEM_NAME_LEN+1];

				printf("Enter new name: ");
				read_line(new_name, ITEM_NAME_LEN);
				item_set_name(item, new_name);
			}
			break;

		case CHANGE_DESCRIPTION:
			printf("Enter new description: ");
			{
				char new_description[DESCRIPTION_MAX+1];

				read_line(new_description, DESCRIPTION_MAX);
				item_set_description(item, new_description);
			}
			break;

		case SHOW_INFO:
			ep_show_item_info(item, true, true);
			break;

		default:
			printf("Option %d not recognised\n", choice);
			break;
		}
	}
}

static Item_list option_clear_list(Item_list item_list, Bag_list bag_list)
{
	if (item_list != NULL) {
		if (bag_list != NULL) {
			bag_list_update_removed_items(bag_list, NULL, 0);  // Remove all items from bags
		}

		item_list_destroy(item_list);
	}

	return NULL;
}

static void option_save_list(Item_list item_list)
{
	if (item_list == NULL) {
		printf("No item list defined yet!\n");
		return;
	}
	if (item_list_total(item_list) < 1) {
		printf("Item list is empty\n");
		return;
	}

	char filename[FILENAME_MAX];

	printf("Enter filename: ");
	read_line(filename, FILENAME_MAX - 1);

	if (filename[0] == '\0') {
		printf("Filename cannot be empty\n");
		return;
	}

	printf("Save as text (t) or binary?\n:");
	if (accept_prompt('t') == true) {
		io_text_save_items_bags_file(filename, item_list, NULL);
	}
	else {
		io_save_items_bags_file(filename, item_list, NULL);
	}
}