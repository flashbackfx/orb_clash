/* classes_chars.c */

#include "editor_public.h"
#include "../adt/player_classes_adt.h"
#include "../adt/pch_list_adt.h"
#include "../adt/player_party_adt.h"
#include "../adt/orb_list_adt.h"
#include "../adt/bag_list_adt.h"
#include "../readline.h"
#include <stdio.h>

Player_classes character_class_editor(Player_classes pc,
                                      Pch_list pl_chars);
Pch_list character_list_editor(Player_classes pc, Pch_list pl_chars,
                               Orb_list orb_list, Bag_list bag_list,
                               Party_list parties);

void classes_chars(Player_classes *pc_ptr, Pch_list *pl_chars_ptr,
                   Orb_list orb_list, Bag_list bag_list,
                   Party_list parties)
{
	enum {BACK = 0, CLASS_LIST_ED, CHAR_LIST_ED, LOAD_FILE};
	int option;

	for (;;) {
		printf("\nClasses and Characters\n");
		printf("(%d-Class list editor, %d-Character List Editor,\n"
		       " %d-Load File, %d-Back)\n:",
		       CLASS_LIST_ED, CHAR_LIST_ED, LOAD_FILE, BACK);
		option = flushed_scanf_d(FSD_ERR_VAL);

		switch (option) {
		case BACK:
			return;

		case CLASS_LIST_ED:
			*pc_ptr = character_class_editor(*pc_ptr, *pl_chars_ptr);
			break;

		case CHAR_LIST_ED:
			*pl_chars_ptr = character_list_editor(*pc_ptr, *pl_chars_ptr,
			                                      orb_list, bag_list,
			                                      parties);
			break;

		case LOAD_FILE:
			ep_option_load_classes_chars_lists(pc_ptr, pl_chars_ptr, parties);
			break;

		default:
			printf("Option %d not recognised\n", option);
			break;
		}
	}
}