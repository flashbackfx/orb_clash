/* editor_public.h */

#ifndef EDITOR_PUBLIC_H
#define EDITOR_PUBLIC_H

#include "../player_class.h"
#include "../adt/player_classes_adt.h"
#include "../adt/player_character_adt.h"
#include "../adt/pch_list_adt.h"
#include "../adt/player_party_adt.h"
#include "../adt/party_list_adt.h"
#include "../adt/spell_adt.h"
#include "../adt/spell_list_adt.h"
#include "../adt/orb_adt.h"
#include "../adt/orb_list_adt.h"
#include "../adt/g_item_adt.h"
#include "../adt/item_list_adt.h"
#include "../adt/bag_adt.h"
#include "../adt/bag_list_adt.h"
#include <stdbool.h>

#define CLASSES_HEADER_STRING \
"ATK    SPD    ITL    MPT    DEF    HPT    hfloor type Name\n"
#define DESCRIPTION_MAX 127


void ep_show_class_list(Player_classes pc, bool show_count);
void ep_show_class_info(struct player_class pclass, bool show_header);
void ep_show_character_list(Pch_list pl_chars, bool show_details);
void ep_show_character_info(Player_character pch, bool show_details,
                            bool show_header);
void ep_show_party_list(Party_list pl_parties, bool show_details);
void ep_show_party_info(Player_party pl_party, bool show_details);
void ep_show_spell_list(Spell_list spell_list, bool show_description);
void ep_show_spell_info(Spell spell, bool show_description, bool show_header);
void ep_show_orb_list(Orb_list orb_list, bool show_contents);
void ep_show_orb_contents(Orb orb, bool show_description);
void ep_show_bag_list(Bag_list bag_list, bool show_contents);
void ep_show_bag_contents(Bag bag, bool show_description);
void ep_show_item_list(Item_list item_list, bool show_description);
void ep_show_item_info(Item item, bool show_description, bool show_header);

struct player_class *ep_choose_class_from_list_obj(Player_classes pc,
                                                   char *operation);
int ep_choose_class_from_list_idx(Player_classes pc, char *operation);
Player_character ep_choose_character_from_list_obj(Pch_list pl_chars,
                                                   char *operation);
int ep_choose_character_from_list_idx(Pch_list pl_chars,char *operation);
Spell ep_choose_spell_from_list_obj(Spell_list spell_list, char *operation);
int ep_choose_spell_from_list_idx(Spell_list spell_list, char *operation);
Item ep_choose_item_from_list_obj(Item_list item_list, char *operation);
int ep_choose_item_from_list_idx(Item_list item_list, char *operation);
Orb ep_choose_orb_from_list_obj(Orb_list orb_list, char *operation);
int ep_choose_orb_from_list_idx(Orb_list orb_list, char *operation);
Bag ep_choose_bag_from_list_obj(Bag_list bag_list, char *operation);
int ep_choose_bag_from_list_idx(Bag_list bag_list, char *operation);
Spell ep_choose_spell_from_orb_obj(Orb orb, char *operation);
int ep_choose_spell_from_orb_idx(Orb orb, char *operation);
Item ep_choose_item_from_bag_obj(Bag bag, char *operation);
int ep_choose_item_from_bag_idx(Bag bag, char *operation);
Player_party ep_choose_party_from_list_obj(Party_list parties, char *operation);
int ep_choose_party_from_list_idx(Party_list parties, char *operation);

void ep_option_remove_class(Player_classes pc, Pch_list pl_chars);
void ep_option_remove_character(Pch_list pl_chars, Party_list pl_parties);
void ep_option_remove_spell(Spell_list spell_list, Orb_list orb_list);
void ep_option_remove_item(Item_list item_list, Bag_list bag_list);
void ep_option_remove_orb(Orb_list orb_list, Pch_list pl_chars);
void ep_option_remove_bag(Bag_list bag_list, Pch_list pl_chars);
void ep_option_remove_spell_from_orb(Orb orb);
void ep_option_remove_item_from_bag(Bag bag);

int ep_enter_character_level(Player_character pch);
int ep_choose_character_orb(Player_character pch, Orb_list orb_list);
int ep_choose_character_bag(Player_character pch, Bag_list bag_list);
int ep_remove_empty_orbs(Orb_list orb_list, Pch_list pl_chars);

void ep_option_load_spells_orbs_lists(Spell_list *spell_list_ptr,
                                      Orb_list *orb_list_ptr,
                                      Pch_list pl_chars);
void ep_option_load_items_bags_lists(Item_list *item_list_ptr,
                                     Bag_list *bag_list_ptr,
                                     Pch_list pl_chars);
void ep_option_load_classes_chars_lists(Player_classes *pc_ptr,
                                        Pch_list *pl_chars_ptr,
                                        Party_list pl_parties);

int compare_ints(const void *a, const void *b);  // Comparison function to be used with qsort

#endif

/**
* This module contains functions that provide user-facing editor
* functionality, like menus and data display, to more than one module.
*/


/**
* SHOW FUNCTIONS
*  Display information about a certain object.
*/

/**
* * void ep_show_class_list(Player_classes pc, bool show_count) *
* * void ep_show_character_list(Pch_list pl_chars, bool show_details) *
* * void ep_show_party_list(Party_list pl_parties, bool show_details) *
* * void ep_show_spell_list(Spell_list spell_list, bool show_description) *
* * void ep_show_orb_list(Orb_list orb_list, bool show_contents) *
* * void ep_show_bag_list(Bag_list bag_list, bool show_contents) *
* * void ep_show_item_list(Item_list item_list, bool show_description) *
*
* These functions display the general lists of their respective types
* given by its first argument. The second argument (of type bool) makes
* the functions show more detailed data if set to "true". These
* functions call the ones documented below to list the objects.
*/

/**
* * void ep_show_party_info(Player_party pl_party, bool show_details) *
* * void ep_show_orb_contents(Orb orb, bool show_description) *
* * void ep_show_bag_contents(Bag bag, bool show_description) *
*
* These functions display information about an object, given by their
* first argument, of their respective type, also listing its associated
* objects. The second argument (of type bool) makes the functions show
* more detailed data if set to "true".
*/

/**
* * void ep_show_class_info(struct player_class pclass, bool show_header) *
* * void ep_show_character_info(Player_character pch, bool show_details,
                                bool show_header) *
* * void ep_show_spell_info(Spell spell, bool show_description,
                            bool show_header) *
* * void ep_show_item_info(Item item, bool show_description,
                           bool show_header) *
*
* These functions display information about an onbject , given by their
* first argument, of their respective type. The second argument (of type
* bool) makes the functions show more detailed data if set to "true"
* (except for ep_show_class_info). 'show_header' makes the function show
* a header before displaying the data if set to "true", which may be
* useful when showing only one item.
*/


/**
* CHOOSE FROM LIST FUNCTIONS
*  Prompt the user to choose an item from a list.
*/

/**
* * struct player_class *ep_choose_class_from_list_obj(Player_classes pc,
                                                       char *operation) *
* * Player_character ep_choose_character_from_list_obj(Pch_list pl_chars,
                                                       char *operation) *
* * Spell ep_choose_spell_from_list_obj(Spell_list spell_list,
                                        char *operation) *
* * Item ep_choose_item_from_list_obj(Item_list item_list,
                                      char *operation) *
* * Orb ep_choose_orb_from_list_obj(Orb_list orb_list,
                                    char *operation) *
* * Bag ep_choose_bag_from_list_obj(Bag_list bag_list,
                                    char *operation) *
* * Spell ep_choose_spell_from_orb_obj(Orb orb,
                                       char *operation) *
* * Item ep_choose_item_from_bag_obj(Bag bag, char *operation) *
* * Player_party ep_choose_party_from_list_obj(Party_list parties,
                                               char *operation) *
*
* These functions list the objects referenced in the list represented in
* the object given by the first argument, and prompt the user to choose
* one. 'operation' is a string meant to contain a name explaining the
* operation that is going to be performed on the choosen object.
* Returns: A reference to the chosen object, or NULL on error.
*/

/**
* * int ep_choose_class_from_list_idx(Player_classes pc,
                                      char *operation) *
* * int ep_choose_character_from_list_idx(Pch_list pl_chars,
                                          char *operation) *
* * int ep_choose_spell_from_list_idx(Spell_list spell_list,
                                      char *operation) *
* * int ep_choose_item_from_list_idx(Item_list item_list,
                                     char *operation) *
* * int ep_choose_orb_from_list_idx(Orb_list orb_list,
                                    char *operation) *
* * int ep_choose_bag_from_list_idx(Bag_list bag_list,
                                    char *operation) *
* * int ep_choose_spell_from_orb_idx(Orb orb, char *operation) *
* * int ep_choose_item_from_bag_idx(Bag bag, char *operation) *
* * int ep_choose_party_from_list_idx(Party_list parties,
* *                                   char *operation) *
*
* These functions work exactly like the ones documented above, only
* differing in the information returned.
* Returns: The index value of the selected object in the list
*  represented in the object given by the first argument, or a negative
*  number on error.
*/


/**
* REMOVE OPTION FUNCTIONS
*  Let the user choose objects from lists that contain them to remove
* said objects from said lists.
*/

/**
* * void ep_option_remove_class(Player_classes pc, Pch_list pl_chars) *
* * void ep_option_remove_character(Pch_list pl_chars, Party_list pl_parties) *
* * void ep_option_remove_spell(Spell_list spell_list, Orb_list orb_list) *
* * void ep_option_remove_item(Item_list item_list, Bag_list bag_list) *
* * void ep_option_remove_orb(Orb_list orb_list, Pch_list pl_chars) *
* * void ep_option_remove_bag(Bag_list bag_list, Pch_list pl_chars) *
* * void ep_option_remove_spell_from_orb(Orb orb) *
* * void ep_option_remove_item_from_bag(Bag bag) *
*
* These functions display all the objects contained in the list given by
* the first argument and let the user select many of them. Once the user
* enters a number corresponding to no listed object, all the previously
* selected objects get removed from the list represented in the object
* given by the first argument. The second argument (if there is one)
* points to a list where the removed objects may be referenced so those
* references can be undone.
*/


/**
* SHARED MENU FUNCTIONS
*  Menus used by more than one module.
*/

/**
* * int ep_enter_character_level(Player_character pch) *
*
* Prompts the user to enter a numerical value and assigns it to the
* level value of the Player_character object given by 'pch'. Note: 'pch'
* is supposed to be valid before using these functions.
* Returns: The new level value for 'pch' or a negative number on error.
*/

/**
* * int ep_choose_character_orb(Player_character pch,
                                Orb_list orb_list) *
* * int ep_choose_character_bag(Player_character pch,
                                Bag_list bag_list) *
*
* These functions ask the user to choose an object from the list given
* by the second argument and equip it to the Player_character object
* given by 'pch'. Note: 'pch' is supposed to be valid before using these
* functions.
* Returns: 0, or a negative number on error.
*/

/**
* * int ep_remove_empty_orbs(Orb_list orb_list, Pch_list pl_chars) *
*
* Removes all empty Orb objects (orbs that have no spells) from
* the Orb_list object given by 'orb_list'. Unreferences these Orb
* objects from all the Player_Character objects listed in the Pch_list
* object 'pl_chars' that may have them referenced. 'pl_chars' can be
* NULL. The removed Orbs get destroyed. 'orb_list' remains empty. This
* function does not use user intervention, it is called by the editors
* instead. The user gets a message if Orb objects get removed.
* Returns: The quantity of removed Orb objects or a negative number on
*  error.
*/


/**
* LOAD OPTION FUNCTIONS
*  Let the user specify a file to be loaded from disk.
*/

/**
* * void ep_option_load_spells_orbs_lists(Spell_list *spell_list_ptr,
                                          Orb_list *orb_list_ptr,
                                          Pch_list pl_chars) *
* * void ep_option_load_items_bags_lists(Item_list *item_list_ptr,
                                         Bag_list *bag_list_ptr,
                                         Pch_list pl_chars) *
* * void ep_option_load_classes_chars_lists(Player_classes *pc_ptr,
                                            Pch_list *pl_chars_ptr,
                                            Party_list pl_parties) *
*
* Each of these functions are meant to be used for a different file type
* accepted by the editor. The user is asked to accept the erasing of the
* data related to the objects pointed to by the first and second
* arguments before continuing. The list represented in the object given
* by the third argument is updated to remove references of the erased
* objects. The user is then prompted for a filename and the type of the
* file (text or binary). The relevant io_files or io_files_text function
* is called. There is no menu for editor_state file loading in this
* module.
*/


/**
* MISC
*/

/**
* * int compare_ints(const void *a, const void *b) *
*
* Compare function to be used with the standard stdlib.h qsort function.
* Compares two integer values.
* Returns: -1 if 'a' is lower than 'b', 0 if 'a' is equal to 'b' and 1
*  if 'a' is higher than 'b'.
*/