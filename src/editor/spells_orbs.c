/* spells_orbs.c */

#include "editor_public.h"
#include "../adt/spell_list_adt.h"
#include "../adt/orb_list_adt.h"
#include "../adt/pch_list_adt.h"
#include "../readline.h"
#include <stdio.h>
#include <stdlib.h>

Spell_list spell_list_editor(Spell_list spell_list, Orb_list orb_list);
Orb_list orbs_manager(Spell_list spell_list, Orb_list orb_list,
                      Pch_list pl_chars);


void spells_orbs(Spell_list *spell_list_ptr, Orb_list *orb_list_ptr,
                 Pch_list pl_chars)
{
	enum {BACK = 0, SPELL_LIST_ED, ORB_MANAGER, LOAD};
	int option;

	for (;;) {
		printf("\nSpells and Orbs\n");
		printf("(%d-Spell list editor, %d-Orbs Manager, %d-Load file\n"
		       " %d-Back)\n:",
		       SPELL_LIST_ED, ORB_MANAGER,LOAD, BACK);
		option = flushed_scanf_d(FSD_ERR_VAL);

		switch (option) {
		case BACK:
			return;

		case SPELL_LIST_ED:
			*spell_list_ptr = spell_list_editor(*spell_list_ptr, *orb_list_ptr);
			break;

		case ORB_MANAGER:
			*orb_list_ptr = orbs_manager(*spell_list_ptr, *orb_list_ptr,
			                             pl_chars);
			break;

		case LOAD:
			ep_option_load_spells_orbs_lists(spell_list_ptr, orb_list_ptr,
			                                 pl_chars);
			break;

		default:
			printf("Option %d not recognised\n", option);
			break;
		}

		ep_remove_empty_orbs(*orb_list_ptr, pl_chars);

		// Destroys empty orb list
		if (*orb_list_ptr != NULL && orb_list_total(*orb_list_ptr) == 0) {
			*orb_list_ptr = orb_list_destroy(*orb_list_ptr);  // returns NULL
		}
	}
}