/* io_files_text.c */

#include "io_files_text.h"
#include "player_class.h"
#include "adt/spell_adt.h"
#include "adt/spell_list_adt.h"
#include "adt/orb_adt.h"
#include "adt/orb_list_adt.h"
#include "adt/g_item_adt.h"
#include "adt/item_list_adt.h"
#include "adt/bag_adt.h"
#include "adt/bag_list_adt.h"
#include "adt/player_classes_adt.h"
#include "adt/player_character_adt.h"
#include "adt/pch_list_adt.h"
#include "editor/editor_public.h"  // DESCRIPTION_MAX macro
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include <stdbool.h>
#include <math.h>  // HUGE_VALF macro... (no need to link the whole library with -lm)

#define HEADER_LEN 8
#define INTSTR_LEN 20  // Maximum number of digits for a decimal integer number
#define FLOATSTR_LEN 40  // Maximum number of digits for a floating-point number, counting separator

enum text_file_types {TEXT_TYPE_ERROR = -1, SPL_ORB, ITM_BAG, CLS_PCH,
                                            NUM_TEXT_FILE_TYPES};

static const char *headers_primary[] =	{[SPL_ORB] = "spellist",
                                         [ITM_BAG] = "itemlist",
                                         [CLS_PCH] = "classes."};

static const char *headers_secondary[] = {[SPL_ORB] = "orb.list",
                                          [ITM_BAG] = "bag.list",
                                          [CLS_PCH] = "charlist"};

// Information provided to text_loader_general by its clients
struct io_t_loader_info {
	enum text_file_types type;
	int (*read_primary)(FILE *, void **);
	int (*read_secondary)(FILE *, void **, const void *);
};

// Information provided to text_saver_general by its clients
struct io_t_saver_info {
	void (*write_primary)(FILE *, const void *);
	void (*write_secondary)(FILE *, const void *, const void *);
};


static int text_loader_general(const struct io_t_loader_info *info,
                               const char *filename, void **primary_list_ptr,
                               void **secondary_list_ptr, const char *caller);
static long text_saver_general(const struct io_t_saver_info *info,
                               const char *filename, const void *primary_list,
                               const void *secondary_list, const char *caller);

static int skip_ws(FILE *fp);
static int advance_until(FILE *fp, char target);
static enum text_file_types get_section_type(FILE *fp, const char *headers[],
                                             int num_headers);
static int get_int(FILE *fp, int *result, bool is_signed);
static int get_float(FILE *fp, float *result);
static int get_string(FILE *fp, char **result, int max_size);
static int get_element_end(FILE *fp);
static void put_int(FILE *fp, int value);
static void put_float(FILE *fp, float value);
static void put_string(FILE *fp, const char *string);
static void put_header(FILE *fp, const char *headers[],
                       enum text_file_types header_idx);

static int read_spell_list_text(FILE *fp, Spell_list *spell_list_ptr);
static int read_orb_list_text(FILE *fp, Orb_list *orb_list_ptr,
                              const Spell_list spell_list);
static int read_item_list_text(FILE *fp, Item_list *item_list_ptr);
static int read_bag_list_text(FILE *fp, Bag_list *bag_list_ptr,
                              const Item_list item_list);
static int read_player_classes_text(FILE *fp, Player_classes *pc_ptr);
static int read_pch_list_text(FILE *fp, Pch_list *pl_chars_ptr,
                              const Player_classes pc);

static void write_spell_list_text(FILE *fp, const Spell_list spell_list);
static void write_orb_list_text(FILE *fp, const Orb_list orb_list,
                                const Spell_list spell_list);
static void write_item_list_text(FILE *fp, const Item_list item_list);
static void write_bag_list_text(FILE *fp, const Bag_list bag_list,
                                const Item_list item_list);
static void write_player_classes_text(FILE *fp, const Player_classes pc);
static void write_pch_list_text(FILE *fp, const Pch_list pl_chars,
                                const Player_classes pc);


int io_text_load_spells_orbs_file(const char *filename,
                                  Spell_list *spell_list_ptr,
                                  Orb_list *orb_list_ptr)
{
	const struct io_t_loader_info info = {
		.type = SPL_ORB,
		.read_primary = (int (*)(FILE *, void **)) read_spell_list_text,
		.read_secondary = (int (*)(FILE *, void **, const void *)) read_orb_list_text,
	};

	return text_loader_general(&info, filename, (void **)spell_list_ptr,
	                           (void **)orb_list_ptr, __func__);
}

int io_text_load_items_bags_file(const char *filename, Item_list *item_list_ptr,
                                 Bag_list *bag_list_ptr)
{
	const struct io_t_loader_info info = {
		.type = ITM_BAG,
		.read_primary = (int (*)(FILE *, void **)) read_item_list_text,
		.read_secondary = (int (*)(FILE *, void **, const void *)) read_bag_list_text,
	};

	return text_loader_general(&info, filename, (void **)item_list_ptr,
	                           (void **)bag_list_ptr, __func__);
}

int io_text_load_classes_chars_file(const char *filename,
                                    Player_classes *pc_ptr,
                                    Pch_list *pl_chars_ptr)
{
	const struct io_t_loader_info info = {
		.type = CLS_PCH,
		.read_primary = (int (*)(FILE *, void **)) read_player_classes_text,
		.read_secondary = (int (*)(FILE *, void **, const void *)) read_pch_list_text,
	};

	return text_loader_general(&info, filename, (void **)pc_ptr,
	                           (void **)pl_chars_ptr, __func__);
}

static int text_loader_general(const struct io_t_loader_info *info,
                               const char *filename, void **primary_list_ptr,
                               void **secondary_list_ptr, const char *caller)
{
	if (filename == NULL) {
		return -2;
	}
	if (filename[0] == '\0') {
		return -3;
	}
	if (primary_list_ptr == NULL) {
		return -4;
	}

	FILE *fp;
	int ch;

	fp = fopen(filename, "r");
	if (fp == NULL) {
		fprintf(stderr, "%s: Could not open file %s\n", caller, filename);
		return -5;
	}

	if (get_section_type(fp, headers_primary, NUM_TEXT_FILE_TYPES) !=
	    info->type)
	{
		fprintf(stderr, "%s: Invalid header\n", caller);
		fclose(fp);
		return -6;
	}

	ch = info->read_primary(fp, primary_list_ptr);

	if (ch != ';') {
		// Finish
		fclose(fp);
		return 0;
	}

	if (secondary_list_ptr == NULL) {
		return -7;
	}

	if (get_section_type(fp, headers_secondary, NUM_TEXT_FILE_TYPES) !=
	    info->type)
	{
		fprintf(stderr, "%s: Invalid header for secondary section\n", caller);
		fclose(fp);
		return -8;
	}

	ch = info->read_secondary(fp, secondary_list_ptr, *primary_list_ptr);

	fclose(fp);
	return 0;
}


long io_text_save_spells_orbs_file(const char *filename,
                                   Spell_list spell_list,
                                   Orb_list orb_list)
{
	const struct io_t_saver_info info = {
		.write_primary = (void (*)(FILE *, const void *)) write_spell_list_text,
		.write_secondary = (void (*)(FILE *, const void *, const void *)) write_orb_list_text,
	};

	return text_saver_general(&info, filename, spell_list, orb_list, __func__);
}

long io_text_save_items_bags_file(const char *filename, Item_list item_list,
                                  Bag_list bag_list)
{
	const struct io_t_saver_info info = {
		.write_primary = (void (*)(FILE *, const void *)) write_item_list_text,
		.write_secondary = (void (*)(FILE *, const void *, const void *)) write_bag_list_text,
	};

	return text_saver_general(&info, filename, item_list, bag_list, __func__);
}

long io_text_save_classes_chars_file(const char *filename, Player_classes pc,
                                     Pch_list pl_chars)
{
	const struct io_t_saver_info info = {
		.write_primary = (void (*)(FILE *, const void *)) write_player_classes_text,
		.write_secondary = (void (*)(FILE *, const void *, const void *)) write_pch_list_text,
	};

	return text_saver_general(&info, filename, pc, pl_chars, __func__);
}

static long text_saver_general(const struct io_t_saver_info *info,
                               const char *filename, const void *primary_list,
                               const void *secondary_list, const char *caller)
{
	if (filename == NULL) {
		return -2;
	}
	if (filename[0] == '\0') {
		return -3;
	}
	if (primary_list == NULL) {
		return -4;
	}

	FILE *fp;
	long file_size;

	fp = fopen(filename, "w");
	if (fp == NULL) {
		fprintf(stderr, "%s: Could not create file %s\n", caller, filename);
		return -5;
	}

	info->write_primary(fp, primary_list);
	if (secondary_list != NULL) {
		info->write_secondary(fp, secondary_list, primary_list);
	}

	file_size = ftell(fp);
	fclose(fp);

	return file_size;
}


/* Skips whitespace, returns the first non-whitespace character or EOF*/
static int skip_ws(FILE *fp)
{
	int ch;

	do {
		ch = getc(fp);
	} while (isspace(ch));

	return ch;
}

/* Skips all characters that are not 'target' and returns 'target' or EOF */
static int advance_until(FILE *fp, char target)
{
	int ch;

	do {
		ch = getc(fp);
	} while (ch != EOF && ch != target);

	return ch;
}

static enum text_file_types get_section_type(FILE *fp, const char *headers[],
                                             int num_headers)
{
	int ch;
	char header_check[HEADER_LEN+1];

	ch = skip_ws(fp);
	if (ch == EOF) {
		// Fail
		return TEXT_TYPE_ERROR;
	}

	header_check[0] = ch;

	for (int i = 1; i < HEADER_LEN; ++i) {
		header_check[i] = getc(fp);
	}
	header_check[HEADER_LEN] = '\0';

	for (int i = 0; i < num_headers; ++i) {
		if (strcmp(header_check, headers[i]) == 0) {
			return i;
		}
	}

	return TEXT_TYPE_ERROR;
}

/*
* Reads the input file skipping leading whitespace, looking for an integer
* value enclosed in brackets that stores in the variable pointed to by result.
* Will read digits up to a defined limit, ignoring the rest. If is_signed is
* true will look for a minus sign at the beginning of the number.
* Returns EOF on failure or the last character read ( '"' ) on success.
*/
static int get_int(FILE *fp, int *result, bool is_signed)
{
	int ch;
	char intstr[INTSTR_LEN+1];
	char *endptr = NULL;
	long lval;
	int ival;

	ch = skip_ws(fp);
	if (ch != '"') {
		fprintf(stderr, "%s: Wrong format for element at %ld\n", __func__,
		                ftell(fp));
		return EOF;
	}

	for (int i = 0; i < INTSTR_LEN; ++i) {
		ch = getc(fp);

		if (isdigit(ch) || (i == 0 && ch == '-' && is_signed == true)) {
			intstr[i] = ch;
		}
		else
		if (ch == '"') {
			intstr[i] = '\0';
			break;
		}
		else {
			fprintf(stderr, "%s: Wrong character (pos %d) for element at %ld\n",
			                __func__, i, ftell(fp));
			return EOF;
		}
	}

	if (ch != '"') {
		fprintf(stderr, "%s: Too many digits in element at %ld\n", __func__,
		               ftell(fp));
		intstr[INTSTR_LEN] = '\0';
		ch = advance_until(fp, '"');
		if (ch == EOF) {
			fprintf(stderr, "%s: Error, file ends inside element\n", __func__);
			return EOF;
		}
	}

	lval = strtol(intstr, &endptr, 10);
	if (endptr == intstr || lval == LONG_MAX || lval == LONG_MIN) {
		fprintf(stderr, "%s: Error reading number %s\n", __func__, intstr);
		return EOF;
	}
	if (lval > INT_MAX) {
		fprintf(stderr, "%s: Error, number higher than maximum value\n",
		                __func__);
		return EOF;
	}

	ival = (int) lval;
	*result = ival;

	return ch;
}

/*
* Reads the input file skipping leading whitespace, looking for a float
* value enclosed in brackets that stores in the variable pointed to by result.
* Will not read negative values, values expressed using e, hexadecimal floats
* or values in the range of doubles. Returns EOF on failure or the last read
* character ( '"' ) on success.
*/
static int get_float(FILE *fp, float *result)
{
	int ch;
	char floatstr[FLOATSTR_LEN+1];
	char *endptr = NULL;
	float fval;
	bool decimal_point = false;

	ch = skip_ws(fp);
	if (ch != '"') {
		fprintf(stderr, "%s: Wrong format for element at %ld\n", __func__,
		                ftell(fp));
		return EOF;
	}

	for (int i = 0; i < FLOATSTR_LEN; ++i) {
		ch = getc(fp);

		if (isdigit(ch)) {
			floatstr[i] = ch;
		}
		else
		if (ch == '.' && decimal_point == false) {
			floatstr[i] = ch;
			decimal_point = true;
		}
		else
		if (ch == '"') {
			floatstr[i] = '\0';
			break;
		}
		else {
			fprintf(stderr, "%s: Wrong character (pos %d) for element at %ld\n",
			                __func__, i, ftell(fp));
			return EOF;
		}
	}

	if (ch != '"') {
		fprintf(stderr, "%s: Too many digits in element at %ld\n", __func__,
		               ftell(fp));
		floatstr[FLOATSTR_LEN] = '\0';
		ch = advance_until(fp, '"');
		if (ch == EOF) {
			fprintf(stderr, "%s: Error, file ends inside element\n", __func__);
			return EOF;
		}
	}

	fval = strtof(floatstr, &endptr);
	if (endptr == floatstr || fval == HUGE_VALF || fval == -HUGE_VALF) {
		fprintf(stderr, "%s: Error reading number %s\n", __func__, floatstr);
		return EOF;
	}

	*result = fval;

	return ch;
}

/*
* Reads the input file skipping leading whitespace, looking for a character
* string enclosed in brackets, up to a length of max_size. There can be any
* character (including control characters) between the brackets except
* brackets. The read string is copied to a dynamically allocated char array
* the address of which is stored in the value pointed to by result. Returns
* EOF on failure and the last character read ( '"' ) on success.
*/
static int get_string(FILE *fp, char **result, int max_size)
{
	int ch;
	char *s;
	int size;

	ch = skip_ws(fp);
	if (ch != '"') {
		fprintf(stderr, "%s: Wrong format for element at %ld\n", __func__,
		                ftell(fp));
		return EOF;
	}

	s = malloc(max_size + 1);
	if (s == NULL) {
		fprintf(stderr, "%s: Could not create string\n", __func__);
		return EOF;
	}

	for (size = 0; size < max_size; ++size) {
		ch = getc(fp);

		if (ch == EOF) {
			fprintf(stderr, "%s: Error, file ends inside element\n", __func__);
			free(s);
			return EOF;
		}
		else
		if (ch == '"') {
			s[size++] = '\0';
			break;
		}

		s[size] = ch;  // Note: Accepts control characters
	}

	if (ch != '"') {
		fprintf(stderr, "%s: Read string is longer than specified maximum\n",
		                __func__);
		s[size] = '\0';
		ch = advance_until(fp, '"');
		if (ch == EOF) {
			// Fail
			fprintf(stderr, "%s: Error, file ends inside element\n", __func__);
			free(s);
			return EOF;
		}
	}

	s = realloc(s, size);
	*result = s;

	return ch;
}

static int get_element_end(FILE *fp)
{
	int ch;

	do {
		ch = getc(fp);
	} while (ch != EOF && ch != ',' && ch != ';');

	return ch;
}

static void put_int(FILE *fp, int value)

{
	fprintf(fp, "\"%d\"", value);
}

static void put_float(FILE *fp, float value)
{
	fprintf(fp, "\"%f\"", value);
}

static void put_string(FILE *fp, const char *string)
{
	fprintf(fp, "\"%s\"", string);
}

static void put_header(FILE *fp, const char *headers[],
                       enum text_file_types header_idx)
{
	fprintf(fp, "%s", headers[header_idx]);
}


static int read_spell_list_text(FILE *fp, Spell_list *spell_list_ptr)
{
	int ch;
	int cost_in_mp;
	char *name;
	char *description;
	Spell spell;

	*spell_list_ptr = spell_list_create();
	if (*spell_list_ptr == NULL) {
		fprintf(stderr, "%s: Could not create Spell_list object\n", __func__);
		return EOF;
	}

	for (;;) {
		ch = get_int(fp, &cost_in_mp, false);
		if (ch != '"') {
			return EOF;
		}

		ch = get_string(fp, &name, SPELL_NAME_LEN);
		if (ch != '"') {
			return EOF;
		}

		ch = get_string(fp, &description, DESCRIPTION_MAX);
		if (ch != '"') {
			return EOF;
		}

		spell = spell_create(name, description, cost_in_mp);
		if (spell == NULL) {
			fprintf(stderr, "%s: Could not create Spell object\n", __func__);
			*spell_list_ptr = spell_list_destroy(*spell_list_ptr);
			return EOF;
		}

		if (spell_list_add(*spell_list_ptr, spell) < 0) {
			fprintf(stderr, "%s: Could not add Spell to list, which has %d\n",
			                __func__, spell_list_total(*spell_list_ptr));
			spell_destroy(spell);
			return EOF;
		}

		free(name);
		free(description);

		ch = get_element_end(fp);
		if (ch != ',') {
			// It was the last element
			break;
		}
	}

	return ch;
}

static int read_orb_list_text(FILE *fp, Orb_list *orb_list_ptr,
                              const Spell_list spell_list)
{
	int ch = '\0';
	char *orb_name;
	Orb orb;
	Spell spell;
	int spell_idx;

	// Note: if first object element group fails we're left with an empty orb
	*orb_list_ptr = orb_list_create();
	if (*orb_list_ptr == NULL) {
		fprintf(stderr, "%s: Could not create Orb_list object\n", __func__);
		return EOF;
	}

	while (ch != EOF) {
		ch = get_string(fp, &orb_name, ORB_NAME_LEN);
		if (ch != '"') {
			return EOF;
		}

		orb = orb_create(orb_name);
		if (orb == NULL) {
			fprintf(stderr, "%s: Could not create Orb object\n", __func__);
			return EOF;
		}

		free(orb_name);

		// Add spells to orb until end of collection
		while (ch != EOF && ch != ',') {
			ch = get_int(fp, &spell_idx, false);
			if (ch != '"') {
				break;
			}

			spell = spell_list_get_spell(spell_list, spell_idx);
			if (spell == NULL) {
				fprintf(stderr, "%s: Spell %d not found in list\n", __func__,
				                spell_idx);
				continue;
			}

			if (orb_add_spell(orb, spell) < 0) {
				fprintf(stderr, "%s: Could not add spell to orb\n",
				                __func__);
			}

			ch = skip_ws(fp);
			if (ch == '"') {
				ungetc(ch, fp);
			}
		}

		if (orb_list_add(*orb_list_ptr, orb) < 0) {
			fprintf(stderr, "%s: Could not add orb to list\n", __func__);
			orb_destroy(orb);
		}
	}

	return ch;
}

/* Almost identical to read_spell_list_text */
static int read_item_list_text(FILE *fp, Item_list *item_list_ptr)
{
	int ch;
	int size_in_bag;
	char *name;
	char *description;
	Item item;

	*item_list_ptr = item_list_create();
	if (*item_list_ptr == NULL) {
		fprintf(stderr, "%s: Could not create Item_list object\n", __func__);
		return EOF;
	}

	for (;;) {
		ch = get_int(fp, &size_in_bag, false);
		if (ch != '"') {
			return EOF;
		}

		ch = get_string(fp, &name, ITEM_NAME_LEN);
		if (ch != '"') {
			return EOF;
		}

		ch = get_string(fp, &description, DESCRIPTION_MAX);
		if (ch != '"') {
			return EOF;
		}

		item = item_create(name, description, size_in_bag);
		if (item == NULL) {
			fprintf(stderr, "%s: Could not create Item object\n", __func__);
			*item_list_ptr = item_list_destroy(*item_list_ptr);
			return EOF;
		}

		if (item_list_add(*item_list_ptr, item) < 0) {
			fprintf(stderr, "%s: Could not add Item to list, which has %d\n",
			                __func__, item_list_total(*item_list_ptr));
			item_destroy(item);
			return EOF;
		}

		free(name);
		free(description);

		ch = get_element_end(fp);
		if (ch != ',') {
			// It was the last element
			break;
		}
	}

	return ch;
}

static int read_bag_list_text(FILE *fp, Bag_list *bag_list_ptr,
                              const Item_list item_list)
{
	int ch = '\0';
	int bag_size;
	char *bag_name;
	Bag bag;
	Item item;
	int item_idx;

	*bag_list_ptr = bag_list_create();
	if (*bag_list_ptr == NULL) {
		fprintf(stderr, "%s: Could not create Bag_list object\n", __func__);
		return EOF;
	}

	while (ch != EOF) {
		ch = get_int(fp, &bag_size, false);
		if (ch != '"') {
			return EOF;
		}

		ch = get_string(fp, &bag_name, BAG_NAME_LEN);
		if (ch != '"') {
			return EOF;
		}

		bag = bag_create(bag_size, bag_name);
		if (bag == NULL) {
			fprintf(stderr, "%s: Could not create Bag object\n", __func__);
			return EOF;
		}

		free(bag_name);

		// Add items to bag until end of collection
		while (ch != EOF && ch != ',') {
			ch = get_int(fp, &item_idx, false);
			if (ch != '"') {
				break;
			}

			item = item_list_get_item(item_list, item_idx);
			if (item == NULL) {
				fprintf(stderr, "%s: Item %d not found in list\n", __func__,
				                item_idx);
				continue;
			}

			if (bag_add_item(bag, item) < 0) {
				fprintf(stderr, "%s: Could not add item to bag\n", __func__);
			}

			ch = skip_ws(fp);
			if (ch == '"') {
				ungetc(ch, fp);
			}
		}

		if (bag_list_add(*bag_list_ptr, bag) < 0) {
			fprintf(stderr, "%s: Could not add bag to list\n", __func__);
			bag_destroy(bag);
		}
	}

	return ch;
}

static int read_player_classes_text(FILE *fp, Player_classes *pc_ptr)
{
	int ch;
	struct player_class cur_pclass;
	char *pclass_name;

	*pc_ptr = player_classes_create();
	if (*pc_ptr == NULL) {
		fprintf(stderr, "%s: Could not create Player_classes object\n",
		                __func__);
		return EOF;
	}

	for (;;) {
		ch = get_int(fp, &cur_pclass.type, true);
		if (ch != '"') {
			return EOF;
		}

		ch = get_float(fp, &cur_pclass.atk);
		if (ch != '"') {
			return EOF;
		}
		ch = get_float(fp, &cur_pclass.spd);
		if (ch != '"') {
			return EOF;
		}
		ch = get_float(fp, &cur_pclass.itl);
		if (ch != '"') {
			return EOF;
		}
		ch = get_float(fp, &cur_pclass.mpt);
		if (ch != '"') {
			return EOF;
		}
		ch = get_float(fp, &cur_pclass.def);
		if (ch != '"') {
			return EOF;
		}
		ch = get_float(fp, &cur_pclass.hpt);
		if (ch != '"') {
			return EOF;
		}

		ch = get_float(fp, &cur_pclass.hpt_floor);
		if (ch != '"') {
			return EOF;
		}

		ch = get_string(fp, &pclass_name, CLASS_NAME_LEN);
		if (ch != '"') {
			return EOF;
		}

		strncpy(cur_pclass.name, pclass_name, CLASS_NAME_LEN);
		cur_pclass.name[CLASS_NAME_LEN] = '\0';
		free(pclass_name);

		if (player_classes_add(*pc_ptr, &cur_pclass) < 0) {
			fprintf(stderr, "%s: Could not add player class to list\n",
			                __func__);
			return EOF;
		}

		ch = get_element_end(fp);
		if (ch != ',') {
			// It was the last element
			break;
		}
	}

	return ch;
}

/* Does not store Orb or Bag data */
static int read_pch_list_text(FILE *fp, Pch_list *pl_chars_ptr,
                              const Player_classes pc)
{
	int ch;
	Player_character pch;
	int pclass_idx;
	int level;
	char *name;

	*pl_chars_ptr = pch_list_create();
	if (*pl_chars_ptr == NULL) {
		fprintf(stderr, "%s: Could not create Pch_list object\n", __func__);
		return EOF;
	}

	for (;;) {
		ch = get_int(fp, &pclass_idx, false);
		if (ch != '"') {
			return EOF;
		}

		ch = get_int(fp, &level, false);
		if (ch != '"') {
			return EOF;
		}

		ch = get_string(fp, &name, CHAR_NAME_LEN);
		if (ch != '"') {
			return EOF;
		}

		pch = player_character_create();
		if (pch == NULL) {
			fprintf(stderr, "%s: Could not create Player_character object\n",
			                __func__);
			free(name);
			return EOF;
		}

		player_character_set_class(pch,
		                           player_classes_get_class(pc, pclass_idx));
		player_character_set_level(pch, level);
		player_character_set_name(pch, name);

		free(name);

		if (pch_list_add(*pl_chars_ptr, pch) < 0) {
			fprintf(stderr, "%s: Could not add Player_character to list\n",
			                __func__);
			player_character_destroy(pch);
			return EOF;
		}

		ch = get_element_end(fp);
		if (ch != ',') {
			// It was the last element
			break;
		}
	}

	return ch;
}

static void write_spell_list_text(FILE *fp, const Spell_list spell_list)
{
	Spell spell;
	int num_spells;

	num_spells = spell_list_total(spell_list);
	if (num_spells < 0) {
		return;
	}

	put_header(fp, headers_primary, SPL_ORB);
	putc('\n', fp);

	for (int i = 0; i < num_spells; ++i) {
		spell = spell_list_get_spell(spell_list, i);

		put_int(fp, spell_get_cost(spell));
		putc(' ', fp);
		put_string(fp, spell_get_name(spell));
		putc(' ', fp);
		put_string(fp, spell_get_description(spell));
		if (i != num_spells - 1) {
			putc(',', fp);
			putc('\n', fp);
		}
	}
}

static void write_orb_list_text(FILE *fp, const Orb_list orb_list,
                                const Spell_list spell_list)
{
	Orb orb;
	int num_orbs;

	num_orbs = orb_list_total(orb_list);
	if (num_orbs < 0) {
		return;
	}

	putc(';', fp);
	putc('\n', fp);
	put_header(fp, headers_secondary, SPL_ORB);
	putc('\n', fp);

	for (int i = 0; i < num_orbs; ++i) {
		int num_spells;

		orb = orb_list_get_orb(orb_list, i);
		num_spells = orb_get_num_spells(orb);

		put_string(fp, orb_get_name(orb));
		putc(' ', fp);

		for (int j = 0; j < num_spells; ++j) {
			put_int(fp, spell_list_find_spell_pos(spell_list,
			                                      orb_get_spell(orb, j)));
		}

		if (i != num_orbs - 1) {
			putc(',', fp);
			putc('\n', fp);
		}
	}
}

static void write_item_list_text(FILE *fp, const Item_list item_list)
{
	Item item;
	int num_items;

	num_items = item_list_total(item_list);
	if (num_items < 0) {
		return;
	}

	put_header(fp, headers_primary, ITM_BAG);
	putc('\n', fp);

	for (int i = 0; i < num_items; ++i) {
		item = item_list_get_item(item_list, i);

		put_int(fp, item_get_size(item));
		putc(' ', fp);
		put_string(fp, item_get_name(item));
		putc(' ', fp);
		put_string(fp, item_get_description(item));
		if (i != num_items - 1) {
			putc(',', fp);
			putc('\n', fp);
		}
	}
}

static void write_bag_list_text(FILE *fp, const Bag_list bag_list,
                                const Item_list item_list)
{
	Bag bag;
	int num_bags;

	num_bags = bag_list_total(bag_list);
	if (num_bags < 0) {
		return;
	}

	putc(';', fp);
	putc('\n', fp);
	put_header(fp, headers_secondary, ITM_BAG);
	putc('\n', fp);

	for (int i = 0; i < num_bags; ++i) {
		int num_items;

		bag = bag_list_get_bag(bag_list, i);
		num_items = bag_get_num_items(bag);

		put_int(fp, bag_get_size(bag));
		putc(' ', fp);
		put_string(fp, bag_get_name(bag));
		putc(' ', fp);

		for (int j = 0; j < num_items; ++j) {
			put_int(fp, item_list_find_item_pos(item_list,
			                                    bag_get_item(bag, j)));
		}

		if (i != num_bags - 1) {
			putc(',', fp);
			putc('\n', fp);
		}
	}
}

static void write_player_classes_text(FILE *fp, const Player_classes pc)
{
	const struct player_class *cur_pclass;
	int num_pclasses;

	num_pclasses = player_classes_total(pc);
	if (num_pclasses < 0) {
		return;
	}

	put_header(fp, headers_primary, CLS_PCH);
	putc('\n', fp);

	for (int i = 0; i < num_pclasses; ++i) {
		cur_pclass = player_classes_get_class(pc, i);

		put_int(fp, cur_pclass->type);
		putc(' ', fp);
		put_float(fp, cur_pclass->atk);
		put_float(fp, cur_pclass->spd);
		put_float(fp, cur_pclass->itl);
		put_float(fp, cur_pclass->mpt);
		put_float(fp, cur_pclass->def);
		put_float(fp, cur_pclass->hpt);
		putc(' ', fp);
		put_float(fp, cur_pclass->hpt_floor);
		putc(' ', fp);
		put_string(fp, cur_pclass->name);

		if (i != num_pclasses - 1) {
			putc(',', fp);
			putc('\n', fp);
		}
	}
}

static void write_pch_list_text(FILE *fp, const Pch_list pl_chars,
                                const Player_classes pc)
{
	Player_character pch;
	int num_chars;

	num_chars = pch_list_total(pl_chars);
	if (num_chars < 0) {
		return;
	}

	putc(';', fp);
	putc('\n', fp);
	put_header(fp, headers_secondary, CLS_PCH);
	putc('\n', fp);

	for (int i = 0; i < num_chars; ++i) {
		const struct player_class *cur_pclass;

		pch = pch_list_get_pch(pl_chars, i);
		cur_pclass = player_character_get_class(pch);

		put_int(fp, player_classes_find_class_pos(pc, cur_pclass));
		putc(' ', fp);
		put_int(fp, player_character_get_level(pch));
		putc(' ', fp);
		put_string(fp, player_character_get_name(pch));

		if (i != num_chars - 1) {
			putc(',', fp);
			putc('\n', fp);
		}
	}
}