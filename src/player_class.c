/* player_class.c */

#include "player_class.h"

const struct player_class *player_class_dummy(void)
{
	static const struct player_class dummy_class = {
		.atk = 0.0f, .spd = 0.0f, .itl = 0.0f,
		.mpt = 0.0f, .def = 0.0f, .hpt = 0.0f,
		.hpt_floor = 0.0f, .type = 0, .name = "--DUMMY--"
	};

	return &dummy_class;
}