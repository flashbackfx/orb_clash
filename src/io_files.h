#ifndef IO_FILES_H
#define IO_FILES_H

#include "adt/spell_list_adt.h"
#include "adt/orb_list_adt.h"
#include "adt/item_list_adt.h"
#include "adt/bag_list_adt.h"
#include "adt/player_classes_adt.h"
#include "adt/pch_list_adt.h"
#include "adt/player_party_adt.h"
#include "adt/party_list_adt.h"

long io_save_spells_orbs_file(const char *filename, Spell_list spell_list,
                              Orb_list orb_list);
long io_save_items_bags_file(const char *filename, Item_list item_list,
                             Bag_list bag_list);
long io_save_classes_chars_file(const char *filename, Player_classes pc,
                                Pch_list pl_chars);
long io_save_editor_state(const char *filename, Spell_list spell_list,
                          Orb_list orb_list, Item_list item_list,
                          Bag_list bag_list, Player_classes pc,
                          Pch_list pl_chars, Party_list pl_parties);

int io_load_spells_orbs_file(const char *filename, Spell_list *spell_list_ptr,
                             Orb_list *orb_list_ptr);
int io_load_items_bags_file(const char *filename, Item_list *item_list_ptr,
                            Bag_list *bag_list_ptr);
int io_load_classes_chars_file(const char *filename, Player_classes *pc_ptr,
                               Pch_list *pl_chars_ptr);
int io_load_editor_state(const char *filename,  Spell_list *spell_list_ptr,
                         Orb_list *orb_list_ptr, Item_list *item_list_ptr,
                         Bag_list *bag_list_ptr, Player_classes *pc_ptr,
                         Pch_list *pl_chars_ptr, Party_list *pl_parties_ptr);

#endif

/**
FILE FORMAT DOCUMENTATION:
 Files saved by this module are binary files.

====
*Spell list file:
	char header[8];                       // File header (marks if file is spell list or spell+orbs list)
	int qty_spells;                       // Quantity of spells in list
	times (qty_spells) {                    // For each spell in list:
		int spell_cost;                       // Spell cost
		char spell_name[SPELL_NAME_LEN+1];    // Name of the spell
		int description_size;                 // Size of the description string (counting null char)
		char description[description_size];   // Description of the spell
	}
====

====
*Spell list + Orb list file:
	Includes an entire spell list plus:

	int qty_orbs;                         // Quantity of orbs in list
	times (qty_orbs) {                      // For each orb in list
		char orb_name[ORB_NAME_LEN+1];        // Name of the orb
		int num_spells;                       // Number of spells in orb
		int spell_idx[num_spells];            // References to spells in the spell list (by position)
	}

 There are no stand-alone Orb list files, they always come with a Spell list.
The load function is the same for both types of files and knows the type by
checking the header.
====

====
*Item list file:
	char header[8];                       // File header (marks if file is item list or items+bags list)
	int qty_items;                        // Quantity of items in list
	times (qty_items) {                     // For each item in list:
		int item_size;                        // Item size
		char item_name[ITEM_NAME_LEN+1];      // Name of the item
		int description_size;                 // Size of the description string (counting null char)
		char description[description_size];   // Description of the item
	}
====

====
*Item list + Bag list file:
	Includes an entire item list plus:

	int qty_bags;                         // Quantity of bags in list
	times (qty_bags) {                      // For each bag in list:
		char name[BAG_NAME_LEN+1];            // Name of bag
		int bag_size;                         // Size of bag
		int num_items;                        // Number of items currently in bag
		int item_idx[num_items];              // References to items in the item list (by position)
	}

 There are no stand-alone Bag list files, they always come with an Item list.
The load function is the same for both types of files and knows the type by
checking the header.
====

====
*Player classes file:
	char header[8];                // File header (marks if file is player classes or player classes+characters list)
	int qty_pclasses;               // Quantity of classes defined in file
	times (qty_pclasses) {            // For each defined class
		struct player_class pclass;     // Class info
	}
====

====
*Player classes + Player characters list file:
	Includes an entire player classes file plus:

	int qty_characters;               // Quantity of characters in list
	times (qty_characters) {            // For each character in list:
		int level;                        // Character level
		int pclass_idx;                   // Character class (referenced by position in the class list)
		int orb_idx;                      // Character orb (referenced by position in the orb list or negative)
		int bag_idx;                      // Character bag (referenced by position in the bag list or negative)
		char name[CHAR_NAME_LEN+1];       // Character name (null-terminated)
	}

 There are no stand-alone Character list files, they always come with a Classes
list. The load function is the same for both types of files and knows the type
by checking the header. The save function does not save orb nor bag info,
so it always stores a negative number in both orb_idx and bag_idx.
 Currently, the save function won't save Pch_list data if the associated
Player_classes object is NULL or empty. Please note that the editor will not
remove Player_character objects from the Pch_list objects that contains them
when the associated player_class info is removed from the Player_classes object
that contains it. These Player_character objects will have a "dummy" class.
====

====
*Editor state file:
	Spell_list+Orb_list file          // An entire Spell_list+Orb_list file
	Item_list+Bag_list file           // An entire Item_list+Bag_list file
	Player_classes+Pch_list file      // An entire Player_classes+Pch_list file (classes and characters lists)
	int num_parties;                  // Number of parties is current Party_list
	times (num_parties) {               // For each party in the list:
		char party_name[PARTY_NAME_LEN+1];  // Party name (null-terminated)
		int num_characters;                 // Number of characters in party
		times (num_characters) {              // For each character in party:
			int pch_idx;                        //Player character (referenced by position in the player charactes list)
			int char_pos;                       // Character position in party
		}
	}

 If qty_spells is negative, there is no more data until the next section and
the resulting Spell_list object is NULL.
 If qty_orbs is negative, there is no more data until the next section and the
resulting Orb_list object is NULL. The editor makes sure there is no Orb
list when there is no Spell list so files with a negative qty_spells value
will have a negative qty_orbs value.
 If qty_items is negative, there is no more data until the next section and the
resulting Item_list object is NULL.
 If qty_bags is negative, there is no more data until the next section and
the resulting Bag_list object is NULL. The editor makes sure there is no Bag
list when there is no Item list so files with a negative qty_items value will
have a negative qty_bags value.
 If qty_classes is negative, there is no more data until the next section and
the resulting Player_classes object is NULL.
 If qty_characters is negative, there is no more data until the next section and
the resulting Pch_list object is NULL. If qty_characters is positive, Orb
and/or Bag data will be saved (as references using positive numbers in
orb_idx and bag_idx respectively) for each character if said data exists
(the Orb and/or Bag are in their respective lists and the Character has them
equipped).
 Finally there's the Party_list data, which will yield NULL if num_parties is
negative. Party_list data can only be saved in the editor state file.
====

********/


/**
* SAVE FUNCTIONS
*/

/**
* * long io_save_spells_orbs_file(const char *filename,
                                  Spell_list spell_list,
                                  Orb_list orb_list) *
*
* Creates a file in the path given by the string 'filename' and saves a
* Spell List+Orb List File with information from 'spell_list' and
* 'orb_list'. If 'orb_list' is NULL only a Spell List File is saved.
* Returns: The result of ftell at the end of the saved file, or a
*  negative number on error.
*/

/**
* * long io_save_items_bags_file(const char *filename,
                                 Item_list item_list,
                                 Bag_list bag_list) *
*
* Creates a file in the path given by the string 'filename' and saves an
* Item List+Bag List File with information from 'item_list' and
* 'bag_list'. If 'bag_list' is NULL only an Item List File is saved.
* Returns: The result of ftell at the end of the saved file, or a
*  negative number on error.
*/

/**
* * long io_save_classes_chars_file(const char *filename,
                                    Player_classes pc,
                                    Pch_list pl_chars) *
*
* Creates a file in the path given by the string 'filename' and saves a
* Player Classes+Player Characers File with information from 'pc' and
* 'pl_chars'. If 'pl_chars' is NULL only a Player Classes File is saved.
* Returns: The result of ftell at the end of the saved file, or a
*  negative number on error.
*/

/**
* * long io_save_editor_state(const char *filename,
                              Spell_list spell_list,
                              Orb_list orb_list, Item_list item_list,
                              Bag_list bag_list, Player_classes pc,
                              Pch_list pl_chars, Party_list pl_parties) *
*
* Creates a file in the path given by the string 'filename' and saves an
* Editor State File with information from 'spell_list', 'orb_list',
* 'item_list', 'bag_list', 'pc', 'pl_chars' and 'pl_parties'.
* Returns: The result of ftell at the end of the saved file, or a
*  negative number on error.
*/


/**
* LOAD FUNCTIONS
*/

/**
* *int io_load_spells_orbs_file(const char *filename,
                                Spell_list *spell_list_ptr,
                                Orb_list *orb_list_ptr) *
*
* Opens the file in the path given by 'filename', and reads
* Spell List or Spell List+Orb List data stored in said file.
* The relevant objects are created in the locations pointed to by
* 'spell_list_ptr' and 'orb_list_ptr'. If the file contains a Spell List,
* 'orb_list_ptr' is not used; if 'orb_list_ptr' is NULL and the file
* contains a Spell List+Orb List, only the Spell List part is
* loaded.
* Returns: 0, or a negative number on error.
*/

/**
* * int io_load_items_bags_file(const char *filename,
                                Item_list *item_list_ptr,
                                Bag_list *bag_list_ptr) *
*
* Opens the file in the path given by 'filename', and reads
* Item List or Item List+Bag List data stored in said file. The
* relevant objects are created in the locations pointed to by
* 'item_list_ptr' and 'bag_list_ptr'. If the file contains an
* Item List, 'bag_list_ptr' is not used; if 'bag_list_ptr' is NULL
* and the file contains an Item List+Bag List, only the Item List part
* is loaded.
* Returns: 0, or a negative number on error.
*/

/**
* * int io_load_classes_chars_file(const char *filename,
                                   Player_classes *pc_ptr,
                                   Pch_list *pl_chars_ptr) *
*
* Opens the file in the path given by 'filename', and reads
* Player Classes or Player Classes+Player Characters data stored in said
* file. The relevant objects are created in the locations pointed to by
* 'pc_ptr' and 'pl_chars_ptr'. If the file only contains Player Classes
* data, 'pl_chars_ptr' is not used; if 'pl_chars_ptr' is NULL and the
* file contains Player Classes+Player Characters data, only the
* Player Classes part is loaded.
* Returns: 0, or a negative number on error.
*/

/**
* * int io_load_editor_state(const char *filename,
                             Spell_list *spell_list_ptr,
                             Orb_list *orb_list_ptr,
                             Item_list *item_list_ptr,
                             Bag_list *bag_list_ptr,
                             Player_classes *pc_ptr,
                             Pch_list *pl_chars_ptr,
                             Party_list *pl_parties_ptr) *
*
* Opens the file in the path given by 'filename', and reads Editor State
* data, creating the relevant objects at the locations pointed to by
* 'spell_list_ptr', 'orb_list_ptr', 'item_list_ptr', 'bag_list_ptr',
* 'pc_ptr', 'pl_chars_ptr' and 'pl_parties_ptr'.
* Returns: 0, or a negative number on error.
*/