/* pch_list_adt.h */

#ifndef PCH_LIST_ADT_H
#define PCH_LIST_ADT_H
#include "player_character_adt.h"
#include "orb_adt.h"
#include "bag_adt.h"
#include "../player_class.h"

struct pch_list_adt;
typedef struct pch_list_adt *Pch_list;


Pch_list pch_list_create(void);
Pch_list pch_list_destroy(Pch_list pl_chars);

int pch_list_add(Pch_list pl_chars, Player_character pch);
int pch_list_remove(Pch_list pl_chars, int position);

int pch_list_total(Pch_list pl_chars);
Player_character pch_list_get_pch(Pch_list pl_chars, int position);
int pch_list_find_pch_pos(Pch_list pl_chars, Player_character pch);

void pch_list_update_stats_base(Pch_list pl_chars);
void pch_list_update_removed_classes(Pch_list pl_chars,
                                     struct player_class *removed_classes[],
                                     int qty_removed);
void pch_list_update_removed_orbs(Pch_list pl_chars, Orb removed_orbs[],
                                  int qty_removed);
void pch_list_update_removed_bags(Pch_list pl_chars, Bag removed_bags[],
                                  int qty_removed);

#endif

/**
* Object that keeps a collection of Player_Character objects. A Pch_list
* object is meant to be a general list of unique Player_character
* objects, removing one from the list erases said Player_character.
*/


/**
* * Pch_list pch_list_create(void) *
*
* Creates a Pch_list object.
* Returns: The created Pch_list object or NULL on error.
*/

/**
* * Pch_list pch_list_destroy(Pch_list pl_chars) *
*
* Destroys the Pch_list object given by 'pl_chars' (if it is not NULL).
* Player_character objects contained in 'pl_chars' are also destroyed.
* Returns: NULL.
*/

/**
* * int pch_list_add(Pch_list pl_chars, Player_character pch) *
*
* Adds the Player_character object given by 'pch' to the end of the list
* represented by the Pch_list object given by 'pl_chars'.
* Returns: Quantity of Player_character objects in 'pl_chars' after the
*  operation if successful or a negative number on error.
*/

/**
* * int pch_list_remove(Pch_list pl_chars, int position) *
*
* Removes the Player_character object in the position given by
* 'position' from the list represented by the Pch_list object given by
* 'pl_chars'. The removed Player_character object is destroyed.
* Returns: Quantity of Player_character objects in 'pl_chars' after the
*  operation if successful or a negative number on error.
*/

/**
* * int pch_list_total(Pch_list pl_chars) *
*
* Returns: Quantity of Player_character objects in the Pch_list given by
*  'pl_chars' or a negative number on error or if 'pl_chars' is NULL.
*/

/**
* * Player_character pch_list_get_pch(Pch_list pl_chars, int position) *
*
* Returns: The Player_character object found in the position given by
*  'position' of the list represented by the Pch_list object given by
*  'pl_chars', or NULL on error.
*/

/**
* * int pch_list_find_pch_pos(Pch_list pl_chars, Player_character pch) *
*
* Returns: The position of the Player_character given by 'pch' in the
*  list represented by the Pch_list object 'pl_chars'. If 'pch' is not
*  in 'pl_chars' or if one or both of them are NULL, a negative number
*  is returned.
*/

/**
* * void pch_list_update_stats_base(Pch_list pl_chars) *
*
* Updates the stats of all the Player_character objects that are in the
* Pch_list 'pl_chars'.
*/

/**
* * void pch_list_update_removed_classes(Pch_list pl_chars,
                                         struct player_class *removed_classes[],
                                         int qty_removed) *
* * void pch_list_update_removed_orbs(Pch_list pl_chars,
                                      Orb removed_orbs[],
                                      int qty_removed) *
* * void pch_list_update_removed_bags(Pch_list pl_chars,
                                      Bag removed_bags[],
                                      int qty_removed) *
*
* Removes references to objects of a type respective to each function
* from Player_character objects contained in the Pch_list 'pl_chars'.
* Those types are struct player_class *, Orb and Bag. The array
* 'removed_...' has 'qty_removed' elements with objects of the relevant
* type that will be un-referenced. If 'removed_...' is NULL, all the
* referenced objects from the relevant type will be removed from all
* Player_character objects in 'pl_chars'. Use these functions before
* destroying one or more object of their respective relevant type so
* Player_character objects do not reference non-existant objects.
*/