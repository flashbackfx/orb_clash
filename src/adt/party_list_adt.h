/* party_list_adt.h */

#ifndef PARTY_LIST_ADT_H
#define PARTY_LIST_ADT_H

#include "player_party_adt.h"
#include "player_character_adt.h"

struct party_list_adt;
typedef struct party_list_adt *Party_list;


Party_list party_list_create(void);
Party_list party_list_destroy(Party_list pl_parties);

int party_list_add(Party_list pl_parties, Player_party party);
int party_list_remove(Party_list pl_parties, int position);
int party_list_total(Party_list pl_parties);

Player_party party_list_get_party(Party_list pl_parties, int position);
int party_list_find_party_pos(Party_list pl_parties, Player_party party);

void party_list_update_removed_chars(Party_list pl_parties,
                                     Player_character removed[],
                                     int qty_removed);

#endif

/**
* Object that keeps a collection of Player_party objects. A Party_list
* object is meant to be a general list of unique Player_party objects,
* removing one from the list erases said Player_party.
*/


/**
* * Party_list party_list_create(void) *
*
* Creates a Party_list object.
* Returns: The created Party_list object or NULL on error.
*/

/**
* * Party_list party_list_destroy(Party_list pl_parties) *
*
* Destroys the Party_list object given by 'pl_parties' (if it is not
* NULL). Player_party objects contained in 'pl_parties' are also
* destroyed.
* Returns: NULL.
*/

/**
* * int party_list_add(Party_list pl_parties, Player_party party) *
*
* Adds the Player_party object given by 'party' to the end of the list
* represented by the Party_list object given by 'pl_parties'.
* Returns: Quantity of Player_party objects in 'pl_parties' after the
*  operation, or a negative number on error.
*/

/**
* * int party_list_remove(Party_list pl_parties, int position) *
*
* Removes the Player_party object in the position given by 'position'
* from the list represented by the Party_list object given by
* 'pl_parties'. The removed Player_party object is destroyed.
* Returns: Quantity of Player_party objects in 'pl_parties' after the
*  operation, or a negative number on error.
*/

/**
* * int party_list_total(Party_list pl_parties) *
*
* Returns: Quantity of Player_party objects in the Party_list object
*  'pl_parties', or a negative number if 'pl_parties' is NULL or there
*  is an error.
*/

/**
* * Player_party party_list_get_party(Party_list pl_parties,
                                      int position) *
*
* Returns: The Player_party object found in the position given by
*  'position' of the list represented by the Party_list object
*  'pl_parties', or NULL on error.
*/

/**
* * int party_list_find_party_pos(Party_list pl_parties,
                                  Player_party party) *
*
* Returns: The position of the Player_party object 'party' in the list
*  represented by the Party_list object 'pl_parties'. If 'party' is not
*  in 'pl_parties' or if one or both of them are NULL, a negative number
*  is returned.
*/

/**
* * void party_list_update_removed_chars(Party_list pl_parties,
                                         Player_character removed[],
                                         int qty_removed) *
*
* Removes references to the Player_character objects contained in the
* array 'removed' that may exist in the Player_party objects contained
* in the Party_list 'pl_parties'. 'qty_removed' is the quantity of
* elements in 'removed'. If 'removed' is NULL, all Player_character
* objects are removed from all Player_party objects in 'pl_parties'.
* Player_party objects may remain empty. Use before destroying the
* relevant Player_character objects so Player_party objects do not
* reference non-existant Player_character objects.
*/