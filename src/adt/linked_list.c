/* linked_list.c: general linked list type implementation */

#include "linked_list.h"
#include <stdio.h>
#include <stdlib.h>

struct list_node {
	void *contents;
	struct list_node *next;
};

struct l_list {
	struct list_node *top;
	int qty_nodes;
	void (*node_destructor)(void *);  // Same format as free (from stdio)
};


struct l_list *l_list_create(void (*node_destructor)(void *))
{
	struct l_list *new_list;

	new_list = malloc(sizeof(*new_list));
	if (new_list == NULL) {
		fprintf(stderr, "%s: Could not create linked list\n", __func__);
		return NULL;
	}

	new_list->qty_nodes = 0;
	new_list->node_destructor = node_destructor;
	new_list->top = NULL;

	return new_list;
}

void *l_list_destroy(struct l_list *list)
{
	if (list != NULL) {
		while (l_list_remove_top(list) > 0) { ; }
		free(list);
	}
	return NULL;
}

int l_list_add_top(struct l_list *list, void *node_contents)
{
	if (list == NULL) {
		fprintf(stderr, "%s: List is NULL\n", __func__);
		return -1;
	}
	if (node_contents == NULL) {
		fprintf(stderr, "%s: node_contents is NULL\n", __func__);
		return -2;
	}

	struct list_node *new_node;

	new_node = malloc(sizeof(*new_node));
	if (new_node == NULL) {
		fprintf(stderr, "%s: Could not add node to list\n", __func__);
		return -3;
	}

	new_node->contents = node_contents;
	new_node->next = list->top;
	list->top = new_node;

	return ++list->qty_nodes;
}

int l_list_add_end(struct l_list *list, void *node_contents)
{
	if (list == NULL) {
		fprintf(stderr, "%s: List is NULL\n", __func__);
		return -1;
	}
	if (node_contents == NULL) {
		fprintf(stderr, "%s: node_contents is NULL\n", __func__);
		return -2;
	}

	struct list_node *new_node;
	struct list_node *cur;

	new_node = malloc(sizeof(*new_node));
	if (new_node == NULL) {
		fprintf(stderr, "%s: Could not add node to list\n", __func__);
		return -3;
	}

	new_node->contents = node_contents;
	new_node->next = NULL;

	cur = list->top;
	if (cur == NULL) {
		list->top = new_node;
	}
	else {
		while (cur->next != NULL) {
			cur = cur->next;
		}
		cur->next = new_node;
	}

	return ++list->qty_nodes;
}

int l_list_remove_top(struct l_list *list)
{
	if (list == NULL) {
		fprintf(stderr, "%s: List is NULL\n", __func__);
		return -1;
	}
	if (list->top == NULL) {
		return 0;
	}

	struct list_node *out;

	out = list->top;
	list->top = list->top->next;

	if (list->node_destructor != NULL) {
		(*list->node_destructor)(out->contents);
	}
	free(out);

	return --list->qty_nodes;
}

int l_list_remove_pos(struct l_list *list, int node_pos)
{
	if (node_pos == 0) {
		return l_list_remove_top(list);
	}
	if (list == NULL) {
		fprintf(stderr, "%s: List is NULL\n", __func__);
		return -1;
	}
	if (node_pos < 0 || node_pos >= list->qty_nodes) {
		fprintf(stderr, "%s: List has no node in position %d\n", __func__,
		                node_pos);
		return -2;
	}

	struct list_node *cur_node = list->top;
	struct list_node *prev_node = NULL;

	while (cur_node != NULL && node_pos > 0) {
		prev_node = cur_node;
		cur_node = cur_node->next;
		--node_pos;
	}
	if (node_pos > 0) {
		fprintf(stderr, "%s: Could not reach desired node, list is malformed\n",
		                __func__);
		return -3;
	}

	prev_node->next = cur_node->next;

	if (list->node_destructor != NULL) {
		(*list->node_destructor)(cur_node->contents);
	}
	free(cur_node);

	return --list->qty_nodes;
}

int l_list_find_node_pos(struct l_list *list, const void *node_contents)
{
	if (list == NULL) {
		fprintf(stderr, "%s: List is NULL\n", __func__);
		return -1;
	}
	if (node_contents == NULL) {
		fprintf(stderr, "%s: node_contents is NULL\n", __func__);
		return -2;
	}

	struct list_node *cur_node = list->top;
	int pos = 0;

	while (cur_node != NULL && pos < list->qty_nodes) {
		if (node_contents == cur_node->contents) {
			return pos;
		}
		cur_node = cur_node->next;
		++pos;
	}

	return -404;  // Contents were not found in list
}

void *l_list_get_node_contents(struct l_list *list, int node_pos)
{
	if (list == NULL) {
		fprintf(stderr, "%s: List is NULL\n", __func__);
		return NULL;
	}
	if (node_pos < 0 || node_pos >= list->qty_nodes) {
		//fprintf(stderr, "%s: List has no node in position %d\n", __func__,
		//                node_pos);
		return NULL;
	}

	struct list_node *cur_node = list->top;

	while (cur_node != NULL && node_pos > 0) {
		cur_node = cur_node->next;
		--node_pos;
	}
	if (node_pos > 0) {
		fprintf(stderr, "%s: Could not reach desired node, list is malformed\n",
		                __func__);
		return NULL;
	}

	return cur_node->contents;
}

/*
* Pointers to the node contents are returned in a dynamic array
* Make sure to free it after using!
* If mode is FLUSH, list is emptied (without using the node destructor)
*/
void **l_list_to_array(struct l_list *list, enum l_list_to_array_options mode)
{
	if (list == NULL) {
		fprintf(stderr, "%s: List is NULL\n", __func__);
		return NULL;
	}

	int num_nodes = list->qty_nodes;
	void **contents_array;
	struct list_node *cur_node = list->top;
	int i = 0;

	if (num_nodes <= 0) {
		fprintf(stderr, "%s: num_nodes is invalid (%d)\n", __func__, num_nodes);
		return NULL;
	}

	contents_array = malloc(sizeof(*contents_array) * num_nodes);  // Note: malloc is able to return an address even if size was 0!
	if (contents_array == NULL) {
		fprintf(stderr, "%s: Could not create contents array\n", __func__);
		return NULL;
	}

	if (mode == L_COPY) {
		while (i < num_nodes && cur_node != NULL) {
			contents_array[i++] = cur_node->contents;
			cur_node = cur_node->next;
		}
	}
	else
	if (mode == L_FLUSH) {
		while (i < num_nodes && cur_node != NULL) {
			contents_array[i++] = cur_node->contents;
			list->top = cur_node->next;
			free(cur_node);
			--list->qty_nodes;
			cur_node = list->top;
		}
	}
	else {
		fprintf(stderr, "%s: Mode %d not recognised\n", __func__, mode);
	}

	if (i < num_nodes) {
		fprintf(stderr, "%s: List is malformed\n", __func__);
	}

	return contents_array;
}

int l_list_qty_nodes(struct l_list *list)
{
	if (list == NULL) {
		fprintf(stderr, "%s: List is NULL\n", __func__);
		return -1;
	}

	return list->qty_nodes;
}