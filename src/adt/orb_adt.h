/* orb_adt.h */

#ifndef ORB_ADT_H
#define ORB_ADT_H

#include "spell_adt.h"

#define ORB_NAME_LEN 15

struct orb_adt;
typedef struct orb_adt *Orb;


Orb orb_create(const char *name);
Orb orb_destroy(Orb orb);

int orb_add_spell(Orb orb, Spell spell);
int orb_remove_spell(Orb orb, int position);

int orb_get_num_spells(Orb orb);
const char *orb_get_name(Orb orb);
Spell orb_get_spell(Orb orb, int position);
int orb_find_spell_pos(Orb orb, Spell spell);

int orb_set_name(Orb orb, const char *name);

#endif

/**
* Orbs are objects that contain Spells and that can be equipped by
* Characters so they can use said Spells in Clash.
*
* Orb objects contain a list that reference Spell objects.
*/


/**
* * Orb orb_create(const char *name) *
*
* Creates an Orb object with a name given by the string 'name'. The
* name of the object won't be longer than ORB_NAME_LEN characters.
* Returns: The created Orb object or NULL on error.
*/

/**
* * Orb orb_destroy(Orb orb) *
*
* Destroys the Orb object given by 'orb' (if it is not NULL).
* Returns: NULL.
*/

/**
* * int orb_add_spell(Orb orb, Spell spell) *
*
* Adds the Spell object given by 'spell' to the end of the list
* represented in the Orb object given by 'orb'.
* Returns: Quantity of Spells in Orb after the operation if
*  successful or a negative number on error.
*/

/**
* * int orb_remove_spell(Orb orb, int position) *
*
* Removes the Spell object in the position given by 'position' from
* the list represented in the Orb object given by 'orb'. Does not
* destroy said Spell object.
* Returns: Quantity of Spells in Orb after the operation if
*  successful or a negative number on error.
*/

/**
* * int orb_get_num_spells(Orb orb) *
*
* Returns: Quantity of Spell objects referenced in the Orb given by
*  'orb' or a negative number if 'orb' is NULL.
*/

/**
* * const char *orb_get_name(Orb orb) *
*
* Returns: The name of the Orb object given by 'orb', or NULL if 'orb'
*  is NULL.
*/

/**
* * int orb_find_spell_pos(Orb orb, Spell spell) *
*
* Returns: The position of the Spell given by 'spell' in the list
*  represented in the Orb given by 'orb'. If 'spell' is not in 'orb' or
*  if one or both of them are NULL, a negative number is returned.
*/

/**
* * Spell orb_get_spell(Orb orb, int position) *
*
* Returns: The Spell object found in the position given by 'position'
*  of the list represented in the Orb object given by 'orb', or NULL
*  on error.
*/

/**
* * int orb_set_name(Orb orb, const char *name) *
*
* Sets the name of the Orb object given by 'orb' to the contents of
* the string given by 'name'. The name won't be longer than
* ORB_NAME_LEN characters. If 'name' is NULL the name of the Orb will
* be empty.
* Returns: A positive number on success, 0 if 'name' is NULL or negative
*  if 'orb' is NULL.
*/