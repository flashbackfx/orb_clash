/* spell_list_adt.c */

#include "spell_list_adt.h"
#include "spell_adt.h"
#include "linked_list.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct spell_list_adt {
	Linked_list list;  // Node contents are of type Spell
};


struct spell_list_adt *spell_list_create(void)
{
	void (*node_destructor)(void *);
	struct spell_list_adt *spell_list;
	Linked_list list;

	spell_list = malloc(sizeof(*spell_list));
	if (spell_list == NULL) {
		fprintf(stderr, "%s: Could not create Spell_list object\n", __func__);
		return NULL;
	}

	node_destructor = (void (*)(void *)) spell_destroy;

	list = l_list_create(node_destructor);
	if (list == NULL) {
		fprintf(stderr, "%s: Could not create Linked_list\n", __func__);
		free(spell_list);
		return NULL;
	}

	spell_list->list = list;

	return spell_list;
}

struct spell_list_adt *spell_list_destroy(struct spell_list_adt *spell_list)
{
	if (spell_list != NULL) {
		l_list_destroy(spell_list->list);
		free(spell_list);
	}
	return NULL;
}

int spell_list_add(struct spell_list_adt *spell_list, Spell spell)
{
	if (spell_list == NULL) {
		fprintf(stderr, "%s: Spell_list object is NULL\n", __func__);
		return -1;
	}
	if (spell == NULL) {
		fprintf(stderr, "%s: Spell object is NULL\n", __func__);
		return -2;
	}

	return l_list_add_end(spell_list->list, spell);
}

int spell_list_remove(struct spell_list_adt *spell_list, int position)
{
	if (spell_list == NULL) {
		fprintf(stderr, "%s: Spell_list object is NULL\n", __func__);
		return -1;
	}

	return l_list_remove_pos(spell_list->list, position);
}

int spell_list_total(struct spell_list_adt *spell_list)
{
	if (spell_list == NULL) {
		fprintf(stderr, "%s: Spell_list object is NULL\n", __func__);
		return -1;
	}

	return l_list_qty_nodes(spell_list->list);
}

Spell spell_list_get_spell(struct spell_list_adt *spell_list, int position)
{
	if (spell_list == NULL) {
		fprintf(stderr, "%s: Spell_list object is NULL\n", __func__);
		return NULL;
	}

	return l_list_get_node_contents(spell_list->list, position);
}

int spell_list_find_spell_pos(struct spell_list_adt *spell_list, Spell spell)
{
	if (spell_list == NULL) {
		fprintf(stderr, "%s: Spell_list object is NULL\n", __func__);
		return -1;
	}
	if (spell == NULL) {
		fprintf(stderr, "%s: Spell object is NULL\n", __func__);
		return -2;
	}

	return l_list_find_node_pos(spell_list->list, spell);
}