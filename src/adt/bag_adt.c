/* bag_adt.c */

#include "bag_adt.h"
#include "g_item_adt.h"
#include "linked_list.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct bag_adt {
	Linked_list list;  // Node contents are of type Item (references)
	int size;          // Capacity of Bag
	int used;          // Used capacity of Bag
	char name[BAG_NAME_LEN+1];  // Used for identification, Bags in game may have no name
};


struct bag_adt *bag_create(int size, const char *name)
{
	if (size < 1) {
		fprintf(stderr, "%s: Size (%d) must be positive\n", __func__, size);
		return NULL;
	}

	struct bag_adt *new_bag;
	Linked_list list;

	new_bag = malloc(sizeof(*new_bag));
	if (new_bag == NULL) {
		fprintf(stderr, "%s: Could not create Bag object\n", __func__);
		return NULL;
	}

	list = l_list_create(NULL);  // Node contents will not be deleted on removal
	if (list == NULL) {
		fprintf(stderr, "%s: Could not create Linked_list\n", __func__);
		free(new_bag);
		return NULL;
	}

	if (name == NULL) {
		name = "";
	}

	new_bag->list = list;
	new_bag->size = size;
	new_bag->used = 0;
	strncpy(new_bag->name, name, BAG_NAME_LEN);
	new_bag->name[BAG_NAME_LEN] = '\0';

	return new_bag;
}

struct bag_adt *bag_destroy(struct bag_adt *bag)
{
	if (bag != NULL) {
		l_list_destroy(bag->list);
		free(bag);
	}

	return NULL;
}

int bag_add_item(struct bag_adt *bag, Item item)
{
	if (bag == NULL) {
		fprintf(stderr, "%s: Bag object is NULL\n", __func__);
		return -1;
	}
	if (item == NULL) {
		fprintf(stderr, "%s: Item object is NULL\n", __func__);
		return -2;
	}

	int item_size = item_get_size(item);

	if (item_size + bag->used > bag->size) {
		fprintf(stderr, "%s: Not enough space in Bag for Item\n", __func__);
		return -3;
	}

	bag->used += item_size;

	return l_list_add_end(bag->list, item);
}

int bag_remove_item(struct bag_adt *bag, int position)
{
	if (bag == NULL) {
		fprintf(stderr, "%s: Bag object is NULL\n", __func__);
		return -1;
	}
	if (l_list_qty_nodes(bag->list) < 1) {
		return 0;  // Bag is empty
	}

	Item removed_item = l_list_get_node_contents(bag->list, position);
	if (removed_item == NULL) {
		return -2;
	}

	bag->used -= item_get_size(removed_item);

	return l_list_remove_pos(bag->list, position);
}

int bag_get_size(struct bag_adt *bag)
{
	if (bag == NULL) {
		fprintf(stderr, "%s: Bag object is NULL\n", __func__);
		return -1;
	}

	return bag->size;
}

int bag_get_used_space(struct bag_adt *bag)
{
	if (bag == NULL) {
		fprintf(stderr, "%s: Bag object is NULL\n", __func__);
		return -1;
	}

	return bag->used;
}

int bag_get_free_space(struct bag_adt *bag)
{
	if (bag == NULL) {
		fprintf(stderr, "%s: Bag object is NULL\n", __func__);
		return -1;
	}

	return bag->size - bag->used;
}

int bag_get_num_items(struct bag_adt *bag)
{
	if (bag == NULL) {
		fprintf(stderr, "%s: Bag object is NULL\n", __func__);
		return -1;
	}

	return l_list_qty_nodes(bag->list);
}

const char *bag_get_name(struct bag_adt *bag)
{
	if (bag == NULL) {
		return NULL;
	}

	return bag->name;
}

Item bag_get_item(struct bag_adt *bag, int position)
{
	if (bag == NULL) {
		fprintf(stderr, "%s: Bag object is NULL\n", __func__);
		return NULL;
	}

	return l_list_get_node_contents(bag->list, position);
}

int bag_find_item_pos(struct bag_adt *bag, Item item)
{
	if (bag == NULL) {
		fprintf(stderr, "%s: Bag object is NULL\n", __func__);
		return -1;
	}
	if (item == NULL) {
		fprintf(stderr, "%s: Item object is NULL\n", __func__);
		return -2;
	}

	return l_list_find_node_pos(bag->list, item);
}

int bag_set_size(struct bag_adt *bag, int size)
{
	if (bag == NULL) {
		fprintf(stderr, "%s: Bag object is NULL\n", __func__);
		return -1;
	}
	if (size < 1) {
		fprintf(stderr, "%s: size (%d) must be positive\n", __func__, size);
		return -2;
	}

	return bag->size = size;
}

int bag_set_name(struct bag_adt *bag, const char *name)
{
	if (bag == NULL) {
		fprintf(stderr, "%s: Bag object is NULL\n", __func__);
		return -1;
	}
	if (name == NULL) {
		name = "";
	}

	strncpy(bag->name, name, BAG_NAME_LEN);
	bag->name[BAG_NAME_LEN] = '\0';

	return 0;
}

struct bag_adt *bag_copy(struct bag_adt *src_bag)
{
	if (src_bag == NULL) {
		fprintf(stderr, "%s: Bag object is NULL\n", __func__);
		return NULL;
	}

	struct bag_adt *new_bag;
	int num_items;

	new_bag = bag_create(src_bag->size, src_bag->name);
	if (new_bag == NULL) {
		fprintf(stderr, "%s:  Could not copy Bag\n", __func__);
		return NULL;
	}

	num_items = l_list_qty_nodes(src_bag->list);

	for (int i = 0; i < num_items; ++i) {
		bag_add_item(new_bag, l_list_get_node_contents(src_bag->list, i));
	}

	return new_bag;
}