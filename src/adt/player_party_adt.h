/* player_party_adt.h */

#ifndef PLAYER_PARTY_ADT_H
#define PLAYER_PARTY_ADT_H

#include "player_character_adt.h"
#include "../player_class.h"

#define PARTY_NAME_LEN 15
#define CLASH_NUM_SIDES 2  // This constant would be more appropiate in a different file, but here reaches both the editors and the clash mode
#define PARTY_NUM_POSITIONS 3

struct player_party_adt;
typedef struct player_party_adt *Player_party;


Player_party player_party_create(void);
Player_party player_party_destroy(Player_party party);

int player_party_add_char(Player_party party, Player_character pch);
Player_character player_party_remove_char(Player_party party,
                                          int char_position);
int player_party_swap_position(Player_party party, int pos_a, int pos_b);

int player_party_get_num_chars(Player_party party);
const char *player_party_get_name(Player_party party);
Player_character player_party_get_char(Player_party party, int char_position);

int player_party_set_name(Player_party party, const char *name);

void player_party_update_removed_characters(Player_party party,
                                            Player_character removed_chars[],
                                            int qty_removed);

#endif

/**
* Player Parties are groups of up to PARTY_NUM_POSITIONS Player
* Characters that come together in on side for Clash.
*
* Player_party objects represent Player Parties. Each Player_character
* object can be in one of the PARTY_NUM_POSITIONS positions of the
* Player_party not occupied by another Player_character.
*/


/**
* * Player_party player_party_create(void) *
*
* Creates a Player_party object.
* Returns: The created Player_party object.
*/

/**
* * Player_party player_party_destroy(Player_party party) *
*
* Destroys the Player_party object given by 'party' (if it is not NULL).
* Referenced Player_character objects are not destroyed.
* Returns: NULL.
*/

/**
* * int player_party_add_char(Player_party party, Player_character pch) *
*
* Adds the Player_character object given by 'pch' to the Player_party
* object given by 'party' in its first free position.
* Returns: The position to where 'pch' was added in 'party', or a
*  negative number on error.
*/

/**
* * Player_character player_party_remove_char(Player_party party,
                                              int position) *
*
* Removes the Player_character object in the position given by
* 'position' from the Player_party object given by 'party'.
* The removed Player_character object is not destroyed.
* Returns: The removed Player_character object, or NULL if there was no
*  Player_character in 'position' or if there was an error.
*/

/**
* * int player_party_swap_position(Player_party party, int pos_a,
                                   int pos_b) *
*
* Swaps the position of the contents in the postition given by 'pos_a'
* and the one given by 'pos_b' of the Player_party object 'party'.
* Each position may be empty.
* Returns: 0, or a negative number on error.
*/

/**
* * int player_party_get_num_chars(Player_party party) *
*
* Returns: Quantity of Player_character objects in the Player_party
*  object 'party', or a negative number or if 'party' is NULL.
*/

/**
* * const char *player_party_get_name(Player_party party) *
*
* Returns: The name of the Player_party object given by 'party', or NULL
*  if 'party' is NULL.
*/

/**
* * Player_character player_party_get_char(Player_party party,
                                           int char_position) *
*
* Returns: The Player_character object found in the position given by
*  'position' of the Player_party object given by 'party', or NULL if
*  said position was empty or there was an error.
*/

/**
* * int player_party_set_name(Player_party party, const char *name) *
*
* Sets the name of the Player_party object given by 'party' to the
* contents of the string given by 'name'. The name won't be longer than
* PARTY_NAME_LEN characters. If 'name' is NULL the name of 'party' will
* be empty.
* Returns: 0 on success, or a negative number on error.
*/

/**
* * void player_party_update_removed_characters(Player_party party,
                                                Player_character removed[],
                                                int qty_removed) *
*
* Removes references to the Player_character objects contained in the
* array 'removed' that may exist in the Player_party object given by
* 'party'. 'qty_removed' is the quantity of elements in 'removed'. If
* 'removed' is NULL, all Player_character objects are removed from
* 'party'. Use before destroying the relevant Player_characte objects so
* 'party' does not reference non-existant Player_characters.
*/