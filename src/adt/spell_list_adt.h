/* spell_list_adt.h */

#ifndef SPELL_LIST_ADT_H
#define SPELL_LIST_ADT_H

#include "spell_adt.h"

struct spell_list_adt;
typedef struct spell_list_adt *Spell_list;


Spell_list spell_list_create(void);
Spell_list spell_list_destroy(Spell_list spell_list);

int spell_list_add(Spell_list spell_list, Spell spell);
int spell_list_remove(Spell_list spell_list, int position);

int spell_list_total(Spell_list spell_list);
Spell spell_list_get_spell(Spell_list spell_list, int position);
int spell_list_find_spell_pos(Spell_list spell_list, Spell spell);

#endif

/**
* Object that keeps a collection of Spell objects. A Spell_list
* object is meant to be a general list of unique Spell objects,
* removing one from the list erases said Spell.
*/


/**
* * Spell_list spell_list_create(void) *
*
* Creates a Spell_list object.
* Returns: The created Spell_list object or NULL on error.
*/

/**
* * Spell_list spell_list_destroy(Spell_list spell_list) *
*
* Destroys the Spell_list object given by 'spell_list' (if it is not
* NULL). Spell objects contained in 'spell_list' are also destroyed.
* Returns: NULL.
*/

/**
* * int spell_list_add(Spell_list spell_list, Spell spell) *
*
* Adds the Spell object given by 'spell' to the end of the list
* represented by the Spell_list object given by 'spell_list'.
* Returns: Quantity of Spell objects in 'spell_list' after the operation,
*  or a negative number on error.
*/

/**
* * int spell_list_remove(Spell_list spell_list, int position) *
*
* Removes the Spell object in the position given by 'position' from
* the list represented by the Spell_list object given by 'spell_list'.
* The removed Spell object is destroyed.
* Returns: Quantity of Player_party objects in 'pl_parties' after the
*  operation, or a negative number on error.
*/

/**
* * int spell_list_total(Spell_list spell_list)*
*
* Returns: Quantity of Spell objects in the Spell_list object
*  'spell_list', or a negative number if 'spell_list' is NULL or there is an
*  error.
*/

/**
* * Spell spell_list_get_spell(Spell_list spell_list, int position) *
*
* Returns: The Spell object found in the position given by 'position'
*  of the list represented by the Spell_list object 'spell_list', or
*  NULL on error.
*/

/**
* * int spell_list_find_spell_pos(Spell_list spell_list, Spell spell) *
*
* Returns: The position of the Spell object 'spell' in the list
*  represented by the Spell_list object 'spell_list'. If 'spell' is not in
*  'spell_list' or if one or both of them are NULL, a negative number is
*  returned.
*/