/* g_item_adt.h */

#ifndef G_ITEM_ADT_H
#define G_ITEM_ADT_H

#define ITEM_NAME_LEN 15

struct g_item_adt;
typedef struct g_item_adt *Item;


Item item_create(const char *name, const char *description, int size);
Item item_destroy(Item item);

int item_get_size(Item item);
const char *item_get_name(Item item);
const char *item_get_description(Item item);

int item_set_size(Item item, int size);
int item_set_name(Item item, const char *name);
int item_set_description(Item item, const char *description);

#endif

/**
* Items (game) are objects that Characters can use in Clash via their
* equipped Bag. Each one has a size value that is used to know how
* much that game Item fills a Bag when the Bag is containing it.
*
* Item objects represent each unique type of game Item.
*/


/**
* * Item item_create(const char *name, const char *description,
                     int size) *
*
* Creates an Item object with a name given by the string 'name', a
* description given by the string 'description' and a size given by
* 'size', which must be positive. The name of the object won't be longer
* than ITEM_NAME_LEN characters.
* Returns: The created Item object or NULL on error.
*/

/**
* * Item item_destroy(Item item) *
*
* Destroys the Item object given by 'item' (if it is not NULL).
* Returns: NULL.
*/

/**
* * int item_get_size(Item item) *
*
* Returns: The size value of the Item object given by 'item', or a
*  negative number if 'item' is NULL.
*/

/**
* * const char *item_get_name(Item item) *
*
* Returns: The name of the Item object given by 'item', or NULL if
*  'item' is NULL.
*/

/**
* * const char *item_get_description(Item item) *
*
* Returns: The description of the Item object given by 'item', or NULL
*  if 'item' is NULL.
*/

/**
* * int item_set_size(Item item, int size) *
*
* Sets the size value of the Item object 'item' to the value of 'size'.
*  Returns: The new size value, or a negative number on error.
*/

/**
* * int item_set_name(Item item, const char *name) *
*
* Sets the name of the Item object given by 'item' to the contents of
* the string given by 'name'. The name won't be longer than
* ITEM_NAME_LEN characters. If 'name' is NULL the name of 'item' will be
* empty.
* Returns: 0 on success, or a negative number on error.
*/

/**
* * int item_set_description(Item item, const char *description) *
*
* Sets the description of the Item object given by 'item' to the
* the contents of the string given by 'description'. If 'description' is
* NULL the description of the Item will be empty.
* Returns: 0 on success, or a negative number on error.
*/