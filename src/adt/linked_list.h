/* linked_list.h: general linked list type */

#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include <stddef.h>

struct l_list;
typedef struct l_list *Linked_list;

enum l_list_to_array_options {L_COPY, L_FLUSH};

Linked_list l_list_create(void (*node_destructor)(void *));
void *l_list_destroy(Linked_list list);
int l_list_add_top(Linked_list list, void *node_contents);
int l_list_add_end(Linked_list list, void *node_contents);
int l_list_remove_top(Linked_list list);
int l_list_remove_pos(Linked_list list, int node_pos);
int l_list_find_node_pos(Linked_list list, const void *node_contents);
void *l_list_get_node_contents(Linked_list list, int node_pos);
void **l_list_to_array(Linked_list list, enum l_list_to_array_options mode);
int l_list_qty_nodes(Linked_list list);

#endif

/**
* Generalized type for linked lists, meant to store data of the same
* type in each of its nodes. Uses a function pointer that can optionally
* be set to point to a function that will be called passing node data
* as its argument when removing said nodes.
* These functions check their arguments, checking if pointers to be
* accessed are NULL, and if numerical (and enumerated) values are in
* range.
*/


/**
* * Linked_list l_list_create(void (*node_destructor)(void *)) *
*
* Creates a Linked_list object. 'node_destructor' is the function that
* will be called when removing nodes. If NULL is passed to as
* 'node_destructor' no function will be called when removing nodes.
* 'node_destructor' has the same form as the free() function from the
* standard stdio.h header.
* Returns: The newly created Linked_list object, or NULL on error.
*/

/**
* * void *l_list_destroy(Linked_list list) *
*
* Destroys the Linked_list object given by 'list' by removing all its
* nodes. If a node_destructor was set during creation, it is called with
* the node contents as its argument for each removed node.
* Returns: NULL
*/

/**
* * int l_list_add_top(Linked_list list, void *node_contents) *
*
* Adds a node with a reference to the data pointed to by 'node_contents'
* to the top of the Linked_list given by 'list'.
* Returns: Quantity of nodes after the operation, or a negative number
*  on error.
*/

/**
* * int l_list_add_end(Linked_list list, void *node_contents) *
*
* Adds a node with a reference to the data pointed to by 'node_contents'
* to the end of the Linked_list given by 'list'.
* Returns: Quantity of nodes in 'list' after the operation, or a
*  negative number on error.
*/

/**
* * int l_list_remove_top(Linked_list list) *
*
* Removes the first node in the Linked_list given by 'list'. If a
* node_destructor was set when creating 'list', it is called using the
* contents of the node as its argument.
* Returns: Quantity of nodes in 'list' after the operation, or a
*  negative number on error.
*/

/**
* * int l_list_remove_pos(Linked_list list, int node_pos) *
*
* Removes the node in the position given by 'node_pos' from the
* Linked_list object given by 'list'. If a node_destructor was set when
* creating 'list', it is called using the contents of the node as its
* argument.
* Returns: Quantity of nodes in 'list' after the operation, or a
*  negative number on error.
*/

/**
* * int l_list_find_node_pos(Linked_list list,
                             const void *node_contents) *
*
* Returns: The position of the node that references the data
*  'node_contents' also points to in the Linked_list object 'list', or a
*  negative number if 'list' has no such node or there was an error.
*/

/**
* * void *l_list_get_node_contents(Linked_list list, int node_pos) *
*
* Returns: A pointer to the data referenced by the node in the position
*  given by 'node_pos' of the Linked_list object 'list', or NULL on
*  error.
*/

/**
* * void **l_list_to_array(Linked_list list,
                           enum l_list_to_array_options mode) *
*
* Creates an array with as many elements as nodes in the Linked_list
* object given by 'list' that contains the contents of said nodes. If
* 'mode' is L_FLUSH, the Linked_list is emptied without calling the
* node_destructor when removing the nodes; if 'mode' is L_COPY, 'list'
* is not changed.
* Returns: A dynamically allocated array of pointers to the data
*  referenced in the nodes of 'list', or NULL on error.
*/

/**
* * int l_list_qty_nodes(Linked_list list) *
*
* Returns: The number of nodes in the Linked_list object given by 'list',
*  or NULL on error.
*/