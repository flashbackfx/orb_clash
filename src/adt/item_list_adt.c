/* item_list_adt.c */

#include "item_list_adt.h"
#include "g_item_adt.h"
#include "linked_list.h"
#include <stdio.h>
#include <stdlib.h>

struct item_list_adt {
	Linked_list list;  // Node contents are of type Item
};


struct item_list_adt *item_list_create(void)
{
	void (*node_destructor)(void *);
	struct item_list_adt *item_list;
	Linked_list list;

	item_list = malloc(sizeof(*item_list));
	if (item_list == NULL) {
		fprintf(stderr, "%s: Could note create Item_list object\n", __func__);
		return NULL;
	}

	node_destructor = (void (*)(void *)) item_destroy;

	list = l_list_create(node_destructor);
	if (list == NULL) {
		fprintf(stderr, "%s: Could not create Linked_list\n", __func__);
		free(item_list);
		return NULL;
	}

	item_list->list = list;

	return item_list;
}

struct item_list_adt *item_list_destroy(struct item_list_adt *item_list)
{
	if (item_list != NULL) {
		l_list_destroy(item_list->list);
		free(item_list);
	}

	return NULL;
}

int item_list_add(struct item_list_adt *item_list, Item item)
{
	if (item_list == NULL) {
		fprintf(stderr, "%s: Item_list object is NULL\n", __func__);
		return -1;
	}
	if (item == NULL) {
		fprintf(stderr, "%s: Item object is NULL\n", __func__);
		return -2;
	}

	return l_list_add_end(item_list->list, item);
}

int item_list_remove(struct item_list_adt *item_list, int position)
{
	if (item_list == NULL) {
		fprintf(stderr, "%s: Item_list object is NULL\n", __func__);
		return -1;
	}

	return l_list_remove_pos(item_list->list, position);
}

int item_list_total(struct item_list_adt *item_list)
{
	if (item_list == NULL) {
		fprintf(stderr, "%s: Item_list object is NULL\n", __func__);
		return -1;
	}

	return l_list_qty_nodes(item_list->list);
}

Item item_list_get_item(struct item_list_adt *item_list, int position)
{
	if (item_list == NULL) {
		fprintf(stderr, "%s: Item_list object is NULL\n", __func__);
		return NULL;
	}

	return l_list_get_node_contents(item_list->list, position);
}

int item_list_find_item_pos(struct item_list_adt *item_list, Item item)
{
	if (item_list == NULL) {
		fprintf(stderr, "%s: Item_list object is NULL\n", __func__);
		return -1;
	}

	return l_list_find_node_pos(item_list->list, item);
}