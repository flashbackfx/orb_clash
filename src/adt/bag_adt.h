/* bag_adt.h */

#ifndef BAG_ADT_H
#define BAG_ADT_H

#include "g_item_adt.h"

#define BAG_NAME_LEN 15

struct bag_adt;
typedef struct bag_adt *Bag;


Bag bag_create(int size, const char *name);
Bag bag_destroy(Bag bag);

int bag_add_item(Bag bag, Item item);
int bag_remove_item(Bag bag, int position);

int bag_get_size(Bag bag);
int bag_get_used_space(Bag bag);
int bag_get_free_space(Bag bag);
int bag_get_num_items(Bag bag);
const char *bag_get_name(Bag bag);
Item bag_get_item(Bag bag, int position);
int bag_find_item_pos(Bag bag, Item item);

int bag_set_size(Bag bag, int size);
int bag_set_name(Bag bag, const char *name);
Bag bag_copy(Bag src_bag);

#endif

/**
* Bags are objects that contain (game) Items, and that can be
* equipped by Characters to use said Items in Clash. Bags have a size
* value that determines how many Items each Bag can carry. The sum of
* the size of each contained Item must not be higher than the size of
* the Bag.
*
* Bag objects contain a list that reference Item objects.
*/


/**
* * Bag bag_create(int size, const char *name) *
*
* Creates a Bag object with a size given by 'size', which must be
* positive, and a name given by the string 'name'. The name of the
* object won't be longer than BAG_NAME_LEN characters.
* Returns: The created Bag object or NULL on error.
*/

/**
* * Bag bag_destroy(Bag bag) *
*
* Destroys the Bag object given by 'bag' (if it is not NULL).
* Returns: NULL.
*/

/**
* * int bag_add_item(Bag bag, Item item) *
*
* Adds the Item object given by 'item' to the end of the list
* represented in the Bag object given by 'bag' taking size values
* into account.
* Returns: Quantity of Items in Bag after the operation if
*  successful or a negative number on error.
*/

/**
* * int bag_remove_item(Bag bag, int position) *
*
* Removes the Item object in the position given by 'position' from
* the list represented in the Bag object given by 'bag', managing
* size values. Does not destroy the removed Item object.
* Returns: Quantity of Items in Bag after the operation if
*  successful or a negative number on error.
*/

/**
* * int bag_get_size(Bag bag) *
*
* Returns: The total size value of the Bag object given by 'bag', or
*  NULL if 'bag' is NULL.
*/

/**
* * int bag_get_used_space(Bag bag) *
*
* Returns: The sum of the size values of every item contained in the
*  Bag object given by 'bag', or NULL if 'bag' is NULL.
*/

/**
* * int bag_get_free_space(Bag bag) *
*
* Returns: The difference between the total size value of the Bag
*  object given by 'bag' and the sum of the size values of every Item
*  it contains, or NULL if 'bag' is NULL.
*/

/**
* * int bag_get_num_items(Bag bag) *
*
* Returns: Quantity of Items in the Bag given by 'bag' or a negative
*  number if 'bag' is NULL.
*/

/**
* * const char *bag_get_name(Bag bag) *
*
* Returns: The name of the Bag object given by 'bag', or NULL if 'bag'
*  is NULL.
*/

/**
* * int bag_find_item_pos(Bag bag, Item item) *
*
* Returns: The position of the Item given by 'item' in the list
*  represented in the Bag given by 'bag'. If 'item' is not in
*  'bag' or if one or both of them are NULL, a negative number is
*  returned.
*/

/**
* * Item bag_get_item(Bag bag, int position) *
*
* Returns: The Item object found in the position given by 'position'
*  of the list represented in the Bag object given by 'bag', or NULL
*  on error.
*/

/**
* * int bag_set_size(Bag bag, int size) *
*
* Sets the size value of the Bag given by 'bag' to the one given by
* 'size'.
* Returns: The value of 'size' on success, or a negative number on error
*  ('bag' was NULL or 'size' was negative).
*/

/**
* * int bag_set_name(Bag bag, const char *name) *
*
* Sets the name of the Bag object given by 'bag' to the contents of
* the string given by 'name'. The name won't be longer than
* BAG_NAME_LEN characters. If 'name' is NULL the name of the Bag
* will be empty.
* Returns: 0 on success, or a negative number on error.
*/

/**
* * Bag bag_copy(Bag src_bag) *
*
* Creates a new Bag object and fills it with the contents of the Bag
* object given by 'src_bag'.
* Returns: The new Bag object, or NULL on error.
*/