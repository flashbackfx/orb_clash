/* pch_list_adt.c */

#include "pch_list_adt.h"
#include "player_character_adt.h"
#include "orb_adt.h"
#include "bag_adt.h"
#include "linked_list.h"
#include "../player_class.h"
#include <stdio.h>
#include <stdlib.h>

struct pch_list_adt {
	Linked_list list;  // Nodes are of Player_character type
};


struct pch_list_adt *pch_list_create(void)
{
	struct pch_list_adt *pl_chars;
	void (*node_destructor)(void *);
	Linked_list list;

	pl_chars = malloc(sizeof(*pl_chars));
	if (pl_chars == NULL) {
		fprintf(stderr, "%s: Could not create Pch_list object\n", __func__);
		return NULL;
	}

	node_destructor = (void (*)(void *)) player_character_destroy;
	list = l_list_create(node_destructor);
	if (list == NULL) {
		fprintf(stderr, "%s: Could not create Linked_list\n", __func__);
		free(pl_chars);
		return NULL;
	}

	pl_chars->list = list;

	return pl_chars;
}

struct pch_list_adt *pch_list_destroy(struct pch_list_adt *pl_chars)
{
	if (pl_chars != NULL) {
		l_list_destroy(pl_chars->list);
		free(pl_chars);
	}

	return NULL;
}

int pch_list_add(struct pch_list_adt *pl_chars, Player_character pch)
{
	if (pl_chars == NULL) {
		fprintf(stderr, "%s: Pch_list object is NULL\n", __func__);
		return -1;
	}
	if (pch == NULL) {
		fprintf(stderr, "%s: Player_character object is NULL\n", __func__);
		return -2;
	}

	return l_list_add_end(pl_chars->list, pch);
}

int pch_list_remove(struct pch_list_adt *pl_chars, int position)
{
	if (pl_chars == NULL) {
		fprintf(stderr, "%s: Pch_list object is NULL\n", __func__);
		return -1;
	}

	return l_list_remove_pos(pl_chars->list, position);
}

int pch_list_total(struct pch_list_adt *pl_chars)
{
	if (pl_chars == NULL) {
		fprintf(stderr, "%s: Pch_list object is NULL\n", __func__);
		return -1;
	}

	return l_list_qty_nodes(pl_chars->list);
}

Player_character pch_list_get_pch(struct pch_list_adt *pl_chars, int position)
{
	if (pl_chars == NULL) {
		fprintf(stderr, "%s: Pch_list object is NULL\n", __func__);
		return NULL;
	}

	return l_list_get_node_contents(pl_chars->list, position);
}

int pch_list_find_pch_pos(struct pch_list_adt *pl_chars, Player_character pch)
{
	if (pl_chars == NULL) {
		fprintf(stderr, "%s: Pch_list object is NULL\n", __func__);
		return -1;
	}

	return l_list_find_node_pos(pl_chars->list, pch);
}

void pch_list_update_stats_base(struct pch_list_adt *pl_chars)
{
	if (pl_chars == NULL) {
		return;
	}

	int num_chars;
	Player_character *chars;  // Dynamic array

	chars = (Player_character *)l_list_to_array(pl_chars->list, L_COPY);
	if (chars == NULL) {
		return;
	}

	num_chars = l_list_qty_nodes(pl_chars->list);

	for (int i = 0; i < num_chars; ++i) {
		player_character_build_stats_base(chars[i]);
	}

	free(chars);
}

/*
* For the _update_removed_... functions receiving NULL as the second argument
* means that all elements from the list have to be updated
*/
void pch_list_update_removed_classes(struct pch_list_adt *pl_chars,
                                     struct player_class *removed_classes[],
                                     int qty_removed)
{
	if (pl_chars == NULL) {
		return;
	}

	int num_chars;
	Player_character cur_pch;

	num_chars = l_list_qty_nodes(pl_chars->list);

	if (removed_classes == NULL) {
		for (int i = 0; i < num_chars; ++i) {
			cur_pch = l_list_get_node_contents(pl_chars->list, i);
			// Set dummy class
			player_character_set_class(cur_pch, player_class_dummy());
		}

		return;
	}

	for (int i = 0; i < num_chars; ++i) {
		for (int j = 0; j < qty_removed; ++j) {
			cur_pch = l_list_get_node_contents(pl_chars->list, i);

			if (player_character_get_class(cur_pch) == removed_classes[j]) {
				// Set dummy class
				player_character_set_class(cur_pch, player_class_dummy());
			}
		}
	}
}

void pch_list_update_removed_orbs(struct pch_list_adt *pl_chars,
                                  Orb removed_orbs[], int qty_removed)
{
	if (pl_chars == NULL) {
		return;
	}

	int num_chars;
	Player_character cur_pch;

	num_chars = l_list_qty_nodes(pl_chars->list);

	if (removed_orbs == NULL) {
		for (int i = 0; i < num_chars; ++i) {
			cur_pch = l_list_get_node_contents(pl_chars->list, i);
			player_character_set_orb(cur_pch, NULL);
		}

		return;
	}

	for (int i = 0; i < num_chars; ++i) {
		for (int j = 0; j < qty_removed; ++j) {
			cur_pch = l_list_get_node_contents(pl_chars->list, i);

			if (player_character_get_orb(cur_pch) == removed_orbs[j]) {
				player_character_set_orb(cur_pch, NULL);
			}
		}
	}
}

void pch_list_update_removed_bags(struct pch_list_adt *pl_chars,
                                  Bag removed_bags[], int qty_removed)
{
	if (pl_chars == NULL) {
		return;
	}

	int num_chars;
	Player_character cur_pch;

	num_chars = l_list_qty_nodes(pl_chars->list);

	if (removed_bags == NULL) {
		for (int i = 0; i < num_chars; ++i) {
			cur_pch = l_list_get_node_contents(pl_chars->list, i);
			player_character_set_bag(cur_pch, NULL);
		}

		return;
	}

	for (int i = 0; i < num_chars; ++i) {
		for (int j = 0; j < qty_removed; ++j) {
			cur_pch = l_list_get_node_contents(pl_chars->list, i);

			if (player_character_get_bag(cur_pch) == removed_bags[j]) {
				player_character_set_bag(cur_pch, NULL);
			}
		}
	}
}