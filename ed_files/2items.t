itemlist
"2" "Potion" "Restores part of an ally HPT stat",
"5" "Super Potion" "Restores a significant part of an ally HPT stat",
"3" "Antidote" "Cures poisoned status",
"7" "Revive" "Brings back a KO'd ally"